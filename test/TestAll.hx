import buddy.Buddy;

class TestAll implements Buddy<[
    scuts.implicit.LocalScopeTest,
    scuts.implicit.ClassScopeTest,
    scuts.implicit.ImportScopeTest,
    scuts.implicit.ExplicitImportScopeTest,
    scuts.implicit.WildcardScopeTest,
    scuts.implicit.ExplicitImportInSameModuleTest,
    scuts.implicit.TypeClassesTest,
    #if performance
    scuts.implicit.PerformanceTest,
    #end
]> {}
