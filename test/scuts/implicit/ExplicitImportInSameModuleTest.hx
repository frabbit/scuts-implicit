package scuts.implicit;

import scuts.implicit.Types;

using buddy.Should;

import scuts.implicit.Wildcard;

import scuts.implicit.ExplicitImportInSameModuleTest.Implicits;

class Implicits {
	@:implicit public static var x : String = "foo";
}

class ExplicitImportInSameModuleTest extends buddy.SingleSuite
{
	public function new () {

		describe("Implicit Resolution", {
			it("should work with explicit implicits from other types in same module", {
				var z:Implicit<String> = _;
				z.should().be("foo");
			});
		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}
}