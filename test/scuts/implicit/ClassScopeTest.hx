package scuts.implicit;

using buddy.Should;

import scuts.implicit.Wildcard;

class ClassScopeTest extends buddy.SingleSuite
{
	public function new () {
		describe("Implicit Resolution Class Scope", {

			it("should consider member scope", expectMemberScope());
			it("should consider static scope", expectStaticScope());
			it("should ignore nested scope and use static scope", expectLocalNestedScopeIsIgnoredAndStaticUsed());

			it("should resolve function arguments of member implicits", {
				var y = Implicit.mk("foo");
				function foo (x:Implicit<Int->String>) {
					x(2).should().be("foo:member:4");
				}
				foo(_);
			});

		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}

	@:implicit static var staticScope = 100;

	@:implicit var memberScope = 2;

	@:implicit var z:String->(Int->String) = function (b:String) return function (a:Int) return b + ":member:" + Std.string(a+a);

	function expectMemberScope()
	{

		identity(_).should().be(memberScope);

	}

	static function expectStaticScope()
	{
		identity(_).should().be(staticScope);
	}

	static function expectLocalNestedScopeIsIgnoredAndStaticUsed()
	{
		{
			var z = Implicit.mk(4);
		}
		identity(_).should().be(staticScope);
	}

	static function expectLocalScopeIsUsedOverStaticScope()
	{
		{
			var z = Implicit.mk(4);
		}
		identity(_).should().be(staticScope);
	}
}