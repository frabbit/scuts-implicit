package scuts.implicit;

using buddy.Should;

import scuts.implicit.Wildcard;

class LocalScopeTest extends buddy.SingleSuite
{
	public function new () {

		describe("Implicit Resolution", {
			it("should consider local scope", {
				var z = Implicit.mk(4);
		
				identity(_).should().be(4);
			});

			it("should ignore nested scope which is out of scope", {
				var z = Implicit.mk(4);
				{
					var z = Implicit.mk(5);
				}
				identity(_).should().be(4);
			});
			
			it("should only consider implicit vars", {
				var z = Implicit.mk(4);
				
				var y = 2;
				identity(_).should().be(4);
			});

			it("should also allow implicit functions", {
				var z = Implicit.mk(function (a:Int) return a + a);
				function foo (x:Implicit<Int->Int>) {
					x(2).should().be(4);
				}
				foo(_);
			});
			
			it("should resolve function arguments of local implicits", {
				var z = Implicit.mk(function (b:String) return function (a:Int) return b + ":local:" + Std.string(a+a) );
				var y = Implicit.mk("foo");
				function foo (x:Implicit<Int->String>) {
					x(2).should().be("foo:local:4");
				}
				foo(_);
			});

			it("should not be done if argument is provided", {
				var x : Implicit<Int> = 10;
				x.should().be(10);
			});

			it("should consider shadowing", {
				var a = Implicit.mk(1);
				identity(_).should().be(1);
				var a = Implicit.mk(2);
				identity(_).should().be(2);
				var a = Implicit.mk(3);
				identity(_).should().be(3);
			});

			#if !java // see https://github.com/HaxeFoundation/haxe/issues/5793
			it("should work with type parameters", {
				var a = Implicit.mk([2]);
				function foo <T> (x:T, expected:Array<T>, a:Implicit<Array<T>>) {
					a.should().containAll(expected);
				}
				foo(1, [2], _);
			});
			
			it("should work with type parameters 2", {
				var a = Implicit.mk([2]);
				function bar <T> (x:T, expected:Array<T>, a:Implicit<Array<T>>) {
					a.should().containAll(expected);
				}
				function foo <T> (x:T, expected:Array<T>, a:Implicit<Array<T>>) {
					bar(x, expected, _);
				}
				foo(1, [2], _);
			});
			#end
			
		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}
}



