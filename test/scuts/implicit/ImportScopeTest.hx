package scuts.implicit;

import scuts.implicit.Implicits1;

import scuts.implicit.Types;

using buddy.Should;

import scuts.implicit.Wildcard;

class ImportScopeTest extends buddy.SingleSuite
{
	public function new () {

		describe("Implicit Resolution", {
			
			it("should get implicit vars from import context", {
				function foo (x:Implicit<String>) {
					x.should().be("outerContext");
				}
				foo(_);
			});

			it("should work with functions and their dependencies", {
				var a = Implicit.mk("foo");
				var b = Implicit.mk(1);
				function foo (expected:Implicit<Void->Bool>) {
					expected().should().be(true);
				}
				foo(_);
			});
	
			it("should work with enums in import context", {
				var x : Implicit<TestEnum> = _;
				x.should().equal(A);
			});

			it("should work with classes in import context", {
				var t : Implicit<TestClass> = _;
				(t.x).should().be(1);
				t.should().beType(TestClass);
			});

			it("should consider functions in import context", {
				function foo (cmp:Implicit<Int->Int->Bool>) {
					cmp(1,2).should().be(false);
					cmp(1,1).should().be(true);
				}
				foo(_);
				function foo (cmp:Implicit<String->String->Bool>) {
					cmp("1","2").should().be(false);
					cmp("1","1").should().be(true);
				}
				foo(_);
			});
		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}
	
	
}



