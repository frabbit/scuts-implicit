package scuts.implicit;

import scuts.implicit.Implicits1.testEnum;
import scuts.implicit.Implicits2.eqFloat;
import scuts.implicit.Implicits2.SubClass.myOption;

using buddy.Should;

import scuts.implicit.Types;

import scuts.implicit.Wildcard;

class ExplicitImportScopeTest extends buddy.SingleSuite
{
	public function new () {

		describe("Implicit Resolution", {
						
			it("should work with explicit imported implicits", {
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				var z:Implicit<haxe.ds.Option<Int>> = _;
				// need to extract the implicit here, see https://github.com/HaxeFoundation/haxe/issues/5790

				var same = (z:haxe.ds.Option<Int>).match(haxe.ds.Option.Some(1));

				same.should().be(true);
			});

			it("should work with explicit imports", {
				var x : Implicit<TestEnum> = _;
				
				x.should().equal(A);
			});

		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}
}