package scuts.implicit;

import scuts.implicit.Types;


class Implicits1 {

	@:implicit public static function eqInt ()return function (a:Int, b:Int):Bool {
		return a == b;
	}

	@:implicit public static function eqString() return function (a:String, b:String) {
		return a == b;
	}

	@:implicit public static var hello:String = "outerContext";

	@:implicit public static var testEnum:TestEnum = A;
	@:implicit public static var testClass:TestClass = new TestClass(1);

	@:implicit public static function helper(a:String):Void->Int return function () {
		return 1;
	}

	@:implicit public static function alotofArguments(helper:Void->Int, a:String, b:String, c:Int, d:Int):Void->Bool return function () {
		return a == b && c == d;
	}

}