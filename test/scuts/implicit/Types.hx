package scuts.implicit;

enum TestEnum {
	A;
	B;
}

class TestClass {
	
	public var x:Int;

	public function new (x:Int) {
		this.x = x;
	}
}