package scuts.implicit;

import scuts.implicit.Implicits2.SubClass2.*;

import scuts.implicit.Types;

using buddy.Should;

import scuts.implicit.Implicit;

import scuts.implicit.Wildcard;

class WildcardScopeTest extends buddy.SingleSuite
{
	
	public function new () {
		
		describe("Implicit Resolution", {

			it("should work with wildcard imports of subtypes", {
				var z:Implicit<haxe.ds.Option<String>> = _;
				// need to extract the implicit here, see https://github.com/HaxeFoundation/haxe/issues/5790

				var same = (z:haxe.ds.Option<String>).match(haxe.ds.Option.Some("foo"));
				same.should().be(true);
			});
			
		});
	}
	static function identity (a:Implicit<Int>) {
		return a;
	}
}