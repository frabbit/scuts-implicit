package scuts.implicit;

using buddy.Should;

import haxe.ds.Option;

import scuts.implicit.Wildcard;

import scuts.implicit.TypeClassesTest.ImplicitInstances;

class Tuple<A,B> {
	public var _1 :A;
	public var _2 : B;

	public function new (a:A, b:B) {
		_1 = a;
		_2 = b;
	}
}

interface Monoid<T> {
	public function concat (a:T, b:T):T;
}

class IntMonoid implements Monoid<Int> {
	public function new () {}
	public function concat (a:Int, b:Int):Int {
		return a + b;
	}
}
class StringMonoid implements Monoid<String> {
	public function new () {}
	public function concat (a:String, b:String):String {
		return a + b;
	}
}

class ArrayMonoid<T> implements Monoid<Array<T>> {
	public function new () {}
	public function concat (a:Array<T>, b:Array<T>):Array<T> {
		return a.concat(b);
	}
}

class OptionMonoid<T> implements Monoid<Option<T>> {
	var monoidT:Monoid<T>;
	public function new (monoidT:Monoid<T>) {
		this.monoidT = monoidT;
	}
	public function concat (a:Option<T>, b:Option<T>):Option<T> {
		return switch [a,b] {
			case [Some(a), Some(b)]: Some(monoidT.concat(a,b));
			case _ : None;

		}
	}
}


class TupleMonoid<A,B> implements Monoid<Tuple<A,B>> {

	var monoidA:Monoid<A>;
	var monoidB:Monoid<B>;

	public function new (monoidA:Monoid<A>, monoidB:Monoid<B>) {
		this.monoidA = monoidA;
		this.monoidB = monoidB;
	}
	public function concat (a:Tuple<A,B>, b:Tuple<A,B>):Tuple<A,B> {
		return new Tuple(monoidA.concat(a._1, b._1), monoidB.concat(a._2, b._2));
	}
}



@:publicFields
class ImplicitInstances {
	@:implicit static var mySuperArrayInt:Monoid<Array<Float>> = cast [1];
	@:implicit static var mySuperArrayString:Monoid<Array<String>> = cast ["foo"];
	@:implicit static var mySuperArrayFloat:Monoid<Array<Float>> = cast [1.1];
	@:implicit static var mySuperArrayFloat2:Monoid<Array<Array<Float>>> = cast [1.1];
	@:implicit static var mySuperArrayFloat3:Monoid<Array<Array<Array<Float>>>> = cast [1.1];
	@:implicit static var intMonoid:Monoid<Int> = new IntMonoid();
	@:implicit static var stringMonoid:Monoid<String> = new StringMonoid();

	@:implicit static function arrayMonoid <T>():Monoid<Array<T>> {
		return new ArrayMonoid();
	}

	@:implicit static function optionMonoid <T>(m:Monoid<T>):Monoid<Option<T>> {
		return new OptionMonoid(m);
	}

	@:implicit static function tupleMonoid <A,B>(m1:Monoid<A>, m2:Monoid<B>):Monoid<Tuple<A,B>> {
		return new TupleMonoid(m1,m2);
	}
}


class TypeClassesTest extends buddy.SingleSuite
{
	static function concat <T>(a:T, b:T, monoid:Implicit<Monoid<T>>) {
		return monoid.concat(a, b);
	}
	public function new () {



		describe("Implicit Resolution", {
			it("should resolve type classes", {

				concat(1,2,_).should().be(3);
				concat("a","b",_).should().be("ab");
				concat([1],[2],_).should().containAll([1,2]);
				concat(Some(1),Some(2),_).should().equal(Some(3));

				var tuple = concat(new Tuple(1,"a"), new Tuple(2,"b"),_);

				tuple._1.should().be(3);
				tuple._2.should().be("ab");

			});

		});
	}

	static function identity (a:Implicit<Int>) {
		return a;
	}
}



