package scuts.implicit;


class Implicits2 {

	@:implicit public static function eqFloat ()return function (a:Float, b:Float):Bool {
		return a == b;
	}
}

class SubClass {

	@:implicit public static function eqFloat ()return function (a:Float, b:Float):Bool {
		return a == b;
	}
	@:implicit public static var myOption = haxe.ds.Option.Some(1); 
}


class SubClass2 {

	@:implicit public static var myOption = haxe.ds.Option.Some("foo"); 
}