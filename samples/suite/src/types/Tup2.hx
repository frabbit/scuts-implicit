package types;

import traits.Eq;

@:structInit class Tup2<A,B> {
	public var _1:A;
	public var _2:B;

	public function new (_1:A, _2:B) {
		this._1 = _1;
		this._2 = _2;
	}

}

class Tup2Eq<A,B> implements Eq<Tup2<A,B>> {

	@:implicit public static function instance <A,B>(eqA:Eq<A>, eqB:Eq<B>) return new Tup2Eq(eqA, eqB);

	var eqA:Eq<A>;
	var eqB:Eq<B>;

	public function new (eqA:Eq<A>, eqB:Eq<B>) {
		this.eqA = eqA;
		this.eqB = eqB;
	}

	public function eq (a:Tup2<A,B>, b:Tup2<A,B>) {
		return eqA.eq(a._1, b._1)
			&& eqB.eq(a._2, b._2);
	}
}