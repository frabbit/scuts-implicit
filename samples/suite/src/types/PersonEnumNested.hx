package types;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;

import instances.StringTraits;
import instances.ArrayTraits;

import traits.Eq;

enum PersonEnumNested {
	WithChildren(name:String, children:Array<PersonEnumNested>);
	WithoutChildren(name:String);
}


class PersonEnumNestedTraits
	implements Eq<PersonEnumNested> {

	@:implicit public static var instance = new PersonEnumNestedTraits();

	var stringEq = null;
	var childrenEq = null;


	public function new () {
		var me = Implicit.mk(this); // shadow instance which is not yet initialized (null)
		this.stringEq = Implicit.implicitByType("Eq<String>");
		this.childrenEq = Implicit.implicitByType("Eq<Array<PersonEnumNested>>");

	}
	public function eq (a:PersonEnumNested, b:PersonEnumNested):Bool {

		return switch [a,b] {
			case [WithChildren(n1, c1), WithChildren(n2, c2)]: stringEq.eq(n1, n2) && childrenEq.eq(c1, c2);
			case [WithoutChildren(n1), WithoutChildren(n2)]: stringEq.eq(n1, n2);
			case _: false;

		}

	}
}