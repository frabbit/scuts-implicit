package types;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;

import instances.StringTraits;
import instances.ArrayTraits;

import traits.Eq;

typedef PersonNested = {
	name : String,
	children:Array<PersonNested>,
}


class PersonNestedTraits
	implements Eq<PersonNested> {

	@:implicit public static var instance = new PersonNestedTraits();

	var stringEq = null;
	var childrenEq = null;


	public function new () {
		var me = Implicit.mk(this); // shadow instance for recursive resolution, which is not yet initialized (null)
		this.stringEq = Implicit.implicitByType("Eq<String>");
		this.childrenEq = Implicit.implicitByType("Eq<Array<PersonNested>>");

	}
	public function eq (a:PersonNested, b:PersonNested):Bool {

		return stringEq.eq(a.name, b.name) && childrenEq.eq(a.children, b.children);
	}
}