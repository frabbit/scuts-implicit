package types;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;

import instances.StringTraits;

import traits.Eq;

typedef Person = {
	name : String,
}

class PersonTraits
	implements Eq<Person> {

	@:implicit public static var instance = new PersonTraits();

	var stringEq = null;

	public function new () {
		this.stringEq = Implicit.implicitByType("Eq<String>");
	}
	public inline function eq (a:Person, b:Person) {
		return stringEq.eq(a.name, b.name);
	}
}