package types.enums;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Lazy;

import traits.Eq;
import traits.Ord;

enum Flex1 {
	Flex1(tail:Array<Flex1>, v:Int);
}


class Flex1Eq
	implements Eq<Flex1> {


	@:implicit public static function instance(eq1:Lazy<Eq<Array<Flex1>>>, eq2:Eq<Int>):Flex1Eq return new Flex1Eq(eq1, eq2);

	var eq2:Eq<Int>;
	var eq1:Lazy<Eq<Array<Flex1>>>;

	public function new (eq1, eq2) {
		this.eq1 = eq1;
		this.eq2 = eq2;
	}

	public inline function eq (a:Flex1, b:Flex1):Bool {
		var eq1 = eq1();
		return switch [a,b] {
			case [Flex1(t1, i1), Flex1(t2, i2)]: eq1.eq(t1, t2) && eq2.eq(i1, i2);
		}
	}
}
