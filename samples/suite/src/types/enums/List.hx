package types.enums;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;

import traits.Eq;
import traits.Ord;

enum List<T> {
	Nil;
	Cons(t:T, tail:List<T>);
}


class ListEq<T>
	implements Eq<List<T>> {

	@:implicit public static function instance<T>(eqT:Eq<T>):ListEq<T> return new ListEq(eqT);

	var eqT = null;
	var eqListT = null;

	public function new (eqT:Eq<T>) {
		var me = Implicit.mk(this);
		this.eqT = eqT;
		eqListT = Implicit.implicitByType("Eq<List<T>>");


	}
	public inline function eq (a:List<T>, b:List<T>):Bool {
		return switch [a,b] {
			case [Nil, Nil]: true;
			case [Cons(e1, t1), Cons(e2, t2)]: eqT.eq(e1, e2) && eqListT.eq(t1, t2);
			case _: false;
		}
	}
}

class ListOrd<T> extends ListEq<T>
	implements Ord<List<T>> {

	@:implicit public static function instance<T>(ordT:Ord<T>):ListOrd<T> return new ListOrd(ordT);

	var ordT = null;
	var ordListT = null;

	public function new (ordT:Ord<T>) {
		super(ordT);
		var me = Implicit.mk(this);
		this.ordT = ordT;
		ordListT = Implicit.implicitByType("Ord<List<T>>");


	}
	public inline function compare (a:List<T>, b:List<T>):Int {
		return switch [a,b] {
			case [Nil, Nil]: 0;
			case [Nil, Cons(_, _)]: -1;
			case [Cons(_, _), Nil]: 1;
			case [Cons(e1, t1), Cons(e2, t2)]: switch ordT.compare(e1, e2) {
				case 0: ordListT.compare(t1, t2);
				case x: x;
			};
		}
	}
}