package types.enums;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Lazy;

import traits.Eq;
import traits.Ord;

enum Flex {
	Flex(tail:Array<Flex>);
}


class FlexEq
	implements Eq<Flex> {


	@:implicit public static function instance(eq1:Lazy<Eq<Array<Flex>>>):FlexEq return new FlexEq(eq1);

	var eq1:Lazy<Eq<Array<Flex>>>;

	public function new (eq1) {
		this.eq1 = eq1;
	}

	public inline function eq (a:Flex, b:Flex):Bool {
		var eq1 = eq1();
		return switch [a,b] {
			case [Flex(t1), Flex(t2)]: eq1.eq(t1, t2);
		}
	}
}
