package types.enums;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Lazy;

import traits.Eq;
import traits.Ord;

enum Flex2 {
	Flex2(tail:Tup2<Flex2, Array<Flex2>>, t:Tup2<String, Flex2>, v:Int);
	Flex2Empty;
}


class Flex2Eq
	implements Eq<Flex2> {

	@:implicit public static function instance(
		eq1:Lazy<Eq<Tup2<Flex2, Array<Flex2>>>>,
		eq2:Lazy<Eq<Tup2<String, Flex2>>>,
		eq3:Eq<Int>
	):Flex2Eq return new Flex2Eq(eq1, eq2, eq3);

	var eq3:Eq<Int>;
	var eq1:Lazy<Eq<Tup2<Flex2, Array<Flex2>>>>;
	var eq2:Lazy<Eq<Tup2<String, Flex2>>>;

	public function new (eq1, eq2, eq3) {
		this.eq1 = eq1;
		this.eq2 = eq2;
		this.eq3 = eq3;
	}

	public inline function eq (a:Flex2, b:Flex2):Bool {
		var eq1 = eq1();
		var eq2 = eq2();
		return switch [a,b] {
			case [Flex2(t1, x1, i1), Flex2(t2, x2, i2)]: eq1.eq(t1, t2) && eq2.eq(x1, x2) && eq3.eq(i1, i2);
			case [Flex2Empty, Flex2Empty]: true;
			case _: false;
		}
	}
}
