package tests.hk;

import tests.Assert.match as m;

import scuts.implicit.Wildcard;

import instances.hk.ArrayTraits;

using traits.hk.Monad;

class MonadTest {
	public static function main () {

		m( [1].flatMap(x -> ["a"], _), ["a"]);
		m( ["a"].flatMap(x -> [1], _), [1]);
		m( ["b"].flatMap(x -> [x+1], _), ["b1"]);

	}
}