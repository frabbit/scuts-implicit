package tests.hk;

import tests.Assert.match as m;

import scuts.implicit.Wildcard;

import instances.hk.ArrayTraits;

using traits.hk.Functor;

class FunctorTest {
	public static function main () {
		m( [1].fmap(x -> "a", _), ["a"]);
		m( ["a"].fmap(x -> 1, _), [1]);
		m( ["b"].fmap(x -> x+1, _), ["b1"]);

	}
}