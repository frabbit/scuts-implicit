package tests;

import scuts.implicit.Wildcard;
import tests.Assert.match as m;
using traits.Eq;

import types.PersonNested;
import types.PersonEnumNested;

import instances.IntTraits;
import instances.ArrayTraits;

import types.enums.List;

class EqTestRecursive {
	public static function main () {

		// person nested

		var p1:PersonNested = { name : "Fred", children : [{ name : "Peter", children: []}]};
		var p2:PersonNested = { name : "Fred", children : [{ name : "Peter", children: []}]};
		m( p1.eq( p2, _), true);

		var p1 = WithChildren("Fred", [WithoutChildren("Peter")]);
		var p2 = WithChildren("Fred", [WithoutChildren("Peter")]);

		m( p1.eq( p2, _), true);

		var l1 = Cons(1, Nil);
		var l2 = Cons(1, Nil);

		m( l1.eq( l2, _), true);

		var l1 = Cons([[1]], Nil);
		var l2 = Cons([[1]], Nil);

		m( l1.eq( l2, _), true);

		var l1 = Cons([[1]], Cons([[2]], Nil));
		var l2 = Cons([[1]], Cons([[2]], Nil));

		m( l1.eq( l2, _), true);


	}
}