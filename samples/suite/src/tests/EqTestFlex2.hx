package tests;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import tests.Assert.match as m;

using traits.Eq;

import instances.ArrayTraits;
import types.Tup2;
import instances.IntTraits;
import instances.StringTraits;

import types.enums.Flex2;

class EqTestFlex2 {
	public static function main () {

		var t = new Tup2("hey", Flex2Empty);
		var f1 = Flex2(new Tup2(Flex2Empty, [Flex2(new Tup2(Flex2Empty, []), t, 2)]), t, 1);
		var f2 = Flex2(new Tup2(Flex2Empty, [Flex2(new Tup2(Flex2Empty, []), t, 2)]), t, 1);

		//var x = Implicit.mk(FlexEq.instance( ));

		m ( EqApi.eq(f1, f2, _), true);

		//var x = () -> Implicit.implicitByType("Int");


		//m( f1.eq( f2, _), true);
		// person nested




	}
}