package tests;

import scuts.implicit.Wildcard;
import tests.Assert.match as m;
using traits.Eq;

import instances.BoolTraits;
import instances.IntTraits;
import instances.StringTraits;
import instances.FloatTraits;
import instances.ArrayTraits;
import types.Person;
import types.Tup2;

class EqTest {
	public static function main () {
		m( true.eq(false, _), false);
		m( 1.eq(2, _), false);
		m( "a".eq("b", _), false);
		m( 2.5.eq(2.6, _), false);

		// arrays
		m( ["a"].eq(["b"], _), false);
		m( [2.5].eq([2.6], _), false);
		m( [false].eq([true], _), false);
		m( [[false]].eq([[true]], _), false);

		m( new Tup2(1, "a").eq(new Tup2(1, "a"), _), true);

		// person
		m( { name : "Fred"}.eq( { name : "Fred"}, _), true);

	}
}