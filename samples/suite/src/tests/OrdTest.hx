package tests;

import scuts.implicit.Wildcard;
import tests.Assert.match as m;
using traits.Ord;

import instances.BoolTraits;
import instances.IntTraits;
import instances.StringTraits;
import instances.FloatTraits;
import instances.ArrayTraits;

class OrdTest {
	public static function main () {
		m( true.compare(false, _), 1);
		m( 1.compare(2, _), -1);
		m( "a".compare("b", _), -1);
		m( 2.5.compare(2.6, _), -1);
		m( [1].compare([1], _), 0);
		m( ["a"].compare(["a"], _), 0);
		m( ["a"].compare(["b"], _), -1);
		m( [["a"]].compare([["b"]], _), -1);
	}
}