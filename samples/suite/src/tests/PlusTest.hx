package tests;

import scuts.implicit.Wildcard;
import tests.Assert.match as m;

using traits.Plus;

import instances.MiscPlusTraits;


class PlusTest {
	public static function main () {
		m( 2.1.plus(1, _), 3.1 );
		m( 2.plus(2.1, _), 4.1 );
		m( 2.plus(1, _), 3 );
		m( 2.plus("hello", _), "2hello" );

	}
}