package tests;

class Assert {
	public static var totals = 0;
	public static var success = 0;
	public static function isTrue (t:Bool) {
		totals++;
		if (!t) trace("assert fails");
		else success++;

	}

	macro public static function match (v:haxe.macro.Expr, v2:haxe.macro.Expr) {
		return macro switch ($v) {
			case $v2: tests.Assert.isTrue(true);
			case _: throw "assert fails";
		}
	}
}