package tests;

import scuts.implicit.Wildcard;
import tests.Assert.match as m;

import instances.BoolTraits;
import instances.IntTraits;
import instances.StringTraits;
import instances.FloatTraits;
import instances.ArrayTraits;

using traits.Show;

class ShowTest {
	public static function main () {
		var t = x -> {trace(x); x; };
		m( true.show(_), "true");
		m( 1.show(_), "1i");
		m( "a".show(_), "a");
		m( 2.5.show(_), "2.5f");
		m( [1].show(_), "[1i]");
		m( ["a"].show(_), "[a]");
		m( [["a"]].show(_), "[[a]]");
		m( [[1]].show(_), "[[1i]]");
	}
}