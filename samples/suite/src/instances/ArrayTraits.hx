package instances;

import scuts.implicit.Wildcard;

import traits.*;

using traits.Ord.OrdApi;
using traits.Eq.EqApi;
using traits.Show.ShowApi;

class ArrayEq<T>
	implements Eq<Array<T>>
{
	@:implicit public static function instance <T>(eqT:Eq<T>):ArrayEq<T> return new ArrayEq(eqT);

	@:implicit var eqT:Eq<T>;

	function new (eqT:Eq<T>) {
		this.eqT = eqT;
	}

	public function eq (a:Array<T>, b:Array<T>) {
		if (a.length != b.length) return false;

		for (i in 0...a.length) {
			if (!a[i].eq(b[i], _)) return false;
		}
		return true;
	}
}

class ArrayOrd<T> extends ArrayEq<T>
	implements Ord<Array<T>>
{
	@:implicit public static function instance <T>(ordT:Ord<T>):ArrayOrd<T> return new ArrayOrd(ordT);

	@:implicit var ordT:Ord<T>;

	function new (ordT:Ord<T>) {
		super(ordT);
		this.ordT = ordT;
	}

	public function compare (a:Array<T>, b:Array<T>) {
		var min = a.length > b.length ? b.length : a.length;

		for (i in 0...min) {
			var c = a[i].compare(b[i], _);
			if (c != 0) return c;
		}

		return if (a.length < b.length) {
			-1;
		} else if (a.length > b.length) {
			1;
		} else {
			0;
		}
	}
}

class ArrayShow<T>
	implements Show<Array<T>>
{
	@:implicit public static function instance <T>(showT:Show<T>):ArrayShow<T> return new ArrayShow(showT);

	@:implicit var showT:Show<T>;

	function new (showT:Show<T>) {
		this.showT = showT;
	}

	public function show (a:Array<T>) {
		return "[" + a.map(x -> x.show(_)).join(",") + "]";
	}
}