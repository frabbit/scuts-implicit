package instances;

import traits.*;

class IntTraits
	implements Eq<Int>
	implements Ord<Int>
	implements Show<Int>
{
	@:implicit public static var instance = new IntTraits();

	function new () {}

	public function eq (a:Int, b:Int) {
		return a == b;
	}
	public function compare (a:Int, b:Int) {
		return a - b;
	}

	public inline function show (a:Int):String {
		return a + "i";
	}

}