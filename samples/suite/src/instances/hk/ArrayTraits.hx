package instances.hk;

import traits.hk.*;

class ArrayTraits
	implements Functor<Array<_>>
	implements Monad<Array<_>>
{
	@:implicit public static var instance = new ArrayTraits();
	public function new () {}

	public function fmap <A,B>(a:Array<A>, f:A->B):Array<B> {
		return a.map(f);
	}

	public function flatMap <A,B>(a:Array<A>, f:A->Array<B>):Array<B> {
		return flatten(a.map(f));
	}

	public function flatten <A,B>(a:Array<Array<A>>):Array<A> {
		return [for (x in a) for (y in x) y];
	}

}