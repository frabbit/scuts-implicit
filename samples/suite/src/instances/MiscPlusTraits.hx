package instances;

import traits.*;

class PlusIntString implements Plus<Int, String, String> {

	@:implicit public static var instance = new PlusIntString();
	public function new () {}
	public function plus (a:Int, b:String):String {
		return a + "" + b;
	}
}

class PlusIntInt implements Plus<Int, Int, Int> {
	@:implicit public static var instance = new PlusIntInt();
	public function new () {}
	public inline function plus (a:Int, b:Int):Int {
		return a + b;
	}
}

class PlusIntFloat implements Plus<Int, Float, Float> {
	@:implicit public static var instance = new PlusIntFloat();
	public function new () {}
	public function plus (a:Int, b:Float):Float {
		return a + b;
	}
}

class PlusFloatInt implements Plus<Float, Int, Float> {

	@:implicit public static var instance = new PlusFloatInt();
	public function new () {}
	public function plus (a:Float, b:Int):Float {
		return a + b;
	}
}