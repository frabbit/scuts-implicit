package instances;

import traits.*;

class StringTraits
	implements Eq<String>
	implements Ord<String>
	implements Show<String>
{
	@:implicit public static var instance = new StringTraits();

	function new () {}

	public inline function eq (a:String, b:String) {
		return a == b;
	}
	public function compare (a:String, b:String) {
		return
			a < b
			? -1
			: a > b
				? 1
				: 0;
	}

	public inline function show (a:String):String {
		return a;
	}
}