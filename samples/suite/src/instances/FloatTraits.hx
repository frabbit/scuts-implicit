package instances;

import traits.*;

class FloatTraits
	implements Eq<Float>
	implements Ord<Float>
	implements Show<Float>
{
	@:implicit public static var instance = new FloatTraits();

	function new () {}

	public function eq (a:Float, b:Float) {
		return a == b;
	}
	public function compare (a:Float, b:Float) {
		return a < b ? -1 : a > b ? 1 : 0;
	}

	public inline function show (a:Float):String {
		return a + "f";
	}

}