package instances;

import traits.*;

class BoolTraits
	implements Eq<Bool>
	implements Ord<Bool>
	implements Show<Bool>
{
	@:implicit public static var instance = new BoolTraits();

	public function new () {}
	public inline function eq (a:Bool, b:Bool):Bool {
		return a == b;
	}

	public inline function compare (a:Bool, b:Bool):Int {
		return switch [a,b] {
			case [true, false]: 1;
			case [false, true]: -1;
			case _: 0;
		}
	}

	public inline function show (a:Bool):String {
		return a ? "true" : "false";
	}
}