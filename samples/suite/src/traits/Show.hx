package traits;

import scuts.implicit.Implicit;

interface Show<T> {
	public function show (a:T):String;
}

class ShowApi {
	public static function show <T>(a:T, show:Implicit<Show<T>>):String {
		return show.show(a);
	}
}
