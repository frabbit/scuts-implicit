package traits;

import scuts.implicit.Implicit;

interface Eq<T> {
	public function eq (a:T, b:T):Bool;
}

class EqApi {
	public static inline function eq <T>(a:T, b:T, eq:Implicit<Eq<T>>):Bool {
		return eq.eq(a,b);
	}
	public static inline function eq2 <T>(a:T, b:T, eq:Eq<T>):Bool {
		return eq.eq(a,b);
	}
}
