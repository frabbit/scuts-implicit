package traits.hk;

import scuts.implicit.Implicit;

interface Functor<F> {
	public function fmap <A,B>(a:F<A>, f:A->B):F<B>;
}
class FunctorApi {
	public static inline function fmap <F,A,B>(a:F<A>, f:A->B, fn:Implicit<Functor<F>>):F<B> {
		return fn.fmap(a, f);
	}
}