package traits.hk;

import scuts.implicit.Implicit;

import traits.hk.Functor;

interface Monad<M> extends Functor<M> {
	public function flatMap <A,B>(a:M<A>, f:A->M<B>):M<B>;
}

class MonadApi {
	public static inline function flatMap <F,A,B>(a:F<A>, f:A->F<B>, fn:Implicit<Monad<F>>):F<B> {
		return fn.flatMap(a, f);
	}
}