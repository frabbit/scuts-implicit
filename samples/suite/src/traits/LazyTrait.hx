package traits;

import traits.Eq;

interface LazyTrait<T,X> {
	public function get (x:X):T
}

class LazyTraitArrayEq<T> implements LazyTrait<Eq<Array<T>>, Eq<T>> {

	@:implicit public static function instance<T> () return new LazyTraitArrayEq();

	public function new () {}

	public function get (eqT:Eq<T>) {
		return instances.ArrayTraits.ArrayEq.instance(eqT);
	}
}