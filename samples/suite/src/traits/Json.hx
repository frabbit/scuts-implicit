interface Json<T> {
	public function fromJson (s:String):T;
	public function toJson (t:T):String;
}