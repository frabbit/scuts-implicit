package;


class Main {
	static function main () {

		tests.EqTestRecursive.main();


		tests.EqTestFlex.main();
		tests.EqTestFlex1.main();
		tests.EqTestFlex2.main();


		tests.EqTest.main();

		tests.OrdTest.main();

		tests.ShowTest.main();
		tests.PlusTest.main();
		tests.hk.FunctorTest.main();
		tests.hk.MonadTest.main();

		trace("success: " + tests.Assert.success + "/" + tests.Assert.totals);
	}
}
