package;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;

import Main.ArrayInstances.instances;

// for nicer syntax
using Main.MonadSyntax;



interface Functor<F> {
	public function map <A,B> (a:F<A>, f:A->B):F<B>;
}

interface Monad<F> extends Functor<F> {
	public function flatMap <A,B> (a:F<A>, f:A->F<B>):F<B>;
	public function pure <A>(a:A):F<A>;
}



class ArrayInstances
	implements Functor<Array<_>>
	implements Monad<Array<_>>
{
	@:implicit public static var instances:Instances<Functor<Array<_>>, Monad<Array<_>>> = new ArrayInstances();


	public function new () {}
	public function map <A,B>(x:Array<A>, f:A->B):Array<B> {
		return x.map(f);
	}
	public function flatMap <A,B> (a:Array<A>, f:A->Array<B>):Array<B> {
		return [for (x in a) for (y in f(x)) y];
	}

	public function pure <A>(a:A):Array<A> return [a];
}

class MonadSyntax {
	public static function flatMap <M,A,B> (a:M<A>, f:A->M<B>, m:Implicit<Monad<M>>):M<B> {
		return m.flatMap(a,f);
	}
}

class Main {
	static function main () {
		trace([1].flatMap( x -> [[x]], _));
	}
}