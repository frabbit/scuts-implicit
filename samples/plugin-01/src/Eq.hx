import haxe.ds.Option;
class Eq {
	public static inline function arrayEq<T>(a:Array<T>, b:Array<T>, eq:T->T->Bool) {
		return if (a == b) { // physical equality implies structural equality
			true;
		} else if (a.length != b.length) {
			false;
		} else {
			var res = true;
			for (i in 0...a.length) {
				if (!eq(a[i], b[i])) {
					res = false;
					break;
				}
			}
			res;
		}
	}

	public static inline function optionEq<T>(a:Option<T>, b:Option<T>, eq:T->T->Bool) {
		return if (a == b) { // physical equality implies structural equality
			true;
		} else switch [a, b] {
			case [Some(t1), Some(t2)]: eq(t1, t2);
			case [None, None]: true;
			case _: false;
		}
	}
	public static function intEq(a:Int, b:Int) return a == b;
}