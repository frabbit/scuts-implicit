

#if macro
import haxe.ds.Option;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.ExprTools;
import haxe.macro.Context;
import scuts.implicit.macros.Data.ResolveContext;
import scuts.implicit.macros.Data.PluginStage;
import scuts.implicit.macros.Resolver;
import scuts.implicit.PluginCache;
import haxe.macro.TypeTools;

class EqPlugin {
	public static function register () {
		scuts.implicit.Plugin.register(resolve, PluginStage.BeforeMembers);
	}

	public static function resolve (ctx:ResolveContext):Option<Expr> {
		var lookup = ctx.idsLookup();
		return if (lookup.exists("Int -> Int -> Bool")) {
			Some(macro Eq.intEq);
		} else if (lookup.exists("Array<_> -> Array<_> -> Bool")) {
			//resolveArrayEq1(ctx); // best inlining
			//resolveArrayEq2(ctx); // easy way
			resolveEqCached(ctx, resolveArrayEq1); // with caching
		} else if (lookup.exists("haxe.ds.Option<_> -> haxe.ds.Option<_> -> Bool")) {
			resolveEqCached(ctx, resolveOptionEq1); // with caching
		} else {
			None;
		}
	}

	// the hard way, use the resolve functionality by yourself
	static function resolveArrayEq1 (ctx:ResolveContext) {
		return resolveNestedEqWithOneDependency(ctx, macro Eq.arrayEq);
	}

	// the hard way, use the resolve functionality by yourself
	static function resolveOptionEq1 (ctx:ResolveContext) {
		return resolveNestedEqWithOneDependency(ctx, macro Eq.optionEq);
	}

	static function resolveNestedEqWithOneDependency (ctx:ResolveContext, baseEqExpr:haxe.macro.Expr) {
		return switch ctx.type() {
			case Some(TFun([a,b], r)):
				// these expressions are only used for typing
				var e1 = Resolver.mkExprWithType(a.t);
				var e2 = Resolver.mkExprWithType(b.t);
				// partially apply known arguments
				var fun = macro $baseEqExpr.bind( $e1, $e2, _); // partially apply 2 of 3 arguments of arrayEq
				// resolve the one and only parameter (dependency) of this function ( T->T->Bool )
				var resolved = Resolver.resolveFirstFunctionArgument(fun);
				var res = macro @:pos(Context.currentPos()) (a, b) -> $baseEqExpr(a,b, (a, b) -> $resolved(a, b) );

				Some(res);
			case _ :
				None;
		}
	}



	// the easy way, just return a function with a parameter that should be resolved
	static function resolveArrayEq2 (ctx:ResolveContext) {
		var res = macro
			@:pos(Context.currentPos())
			(c) ->  (a, b) -> Eq.arrayEq(a,b, (a1, b1) -> c(a1,b1));

		return Some(res);
	}


	// use a cache
	static var cache = PluginCache.get();

	static function resolveEqCached (ctx:ResolveContext, resolve:ResolveContext->Option<Expr>) {
		return cache.getOrSet(ctx.id(), resolve.bind(ctx));
	}
}

#end