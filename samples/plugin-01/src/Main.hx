import scuts.implicit.Implicit;
import scuts.implicit.Wildcard;
import haxe.ds.Option;

class Main {
	static function main () {

		var eqString = Implicit.mk( (s1:String, s2:String) -> s1 == s2);

		trace(eq([1], [1], _));
		trace(eq([[1]], [[1]], _)); // for free
		trace(eq([[[1]]], [[[1]]], _)); // for free
		trace(eq(["1"], ["1"], _)); // uses the eqString implicit in combination with the generated arrayEq
		trace(eq([[["1"]]], [[["1"]]], _)); // for free
		trace(eq([[[[["1"]]]]], [[[[["1"]]]]], _)); // for free
		trace(eq([[[[["1"]]]]], [[[[["2"]]]]], _)); // for free
		trace(eq([[Some([[["1"]]])]], [[Some([[["1"]]])]], _)); // for free
		trace(eq([[Some([[["1"]]])]], [[None]], _)); // for free
		trace(eq(Some(["1"]), None, _)); // for free
		trace(eq(Some(["1"]), Some(["1"]), _)); // for free

		var localEq:Implicit<Option<Array<String>>->Option<Array<String>>->Bool> = _;

		trace(eq(Some(["1"]), Some(["1"]), _)); // uses localEq
		trace(eq(Some(["1"]), Some(["1"]), _)); // uses localEq
	}

	static inline function eq <T>(a:T, b:T, eq:Implicit<T->T->Bool>) {
		return eq(a, b);
	}
}
