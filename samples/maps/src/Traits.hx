import scuts.implicit.Implicit;

interface ShallowCopy<T> {
	public function shallowCopy (t:T):T;
}

interface Functor<F> {
	public function map <A,B> (a:F<A>, f:A->B):F<B>;
}

interface MapAccess<M,K> {
	function get <V>(m:M<V>, key:K):V;
	function set <V>(m:M<V>, key:K, v:V):Void;
}


@:publicFields
class MapAccessSyntax {
	static inline function get <M,K,V>(m:M<V>, key:K, im:Implicit<MapAccess<M,K>>):V {
		return im.get(m,key);
	}
	static inline function set <M,K,V>(m:M<V>, key:K, v:V, im:Implicit<MapAccess<M,K>>):Void {
		return im.set(m, key, v);
	}
}
@:publicFields class FunctorSyntax {
	static inline function map <M,A,B> (a:M<A>, f:A->B, m:Implicit<Functor<M>>):M<B> {
		return m.map(a,f);
	}
}

@:publicFields class ShallowCopySyntax {
	static inline function shallowCopy <T>(t:T, sc:Implicit<ShallowCopy<T>>):T {
		return sc.shallowCopy(t);
	}
}