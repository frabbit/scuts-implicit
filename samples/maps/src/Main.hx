import scuts.implicit.Wildcard;

import IntMap.IntMapAsShallowCopy.instance;
import IntMap.IntMapAsMapAccess.instance;
import IntMap.IntMapAsFunctor.instance;

import StringMap.StringMapAsShallowCopy.instance;
import StringMap.StringMapAsMapAccess.instance;
import StringMap.StringMapAsFunctor.instance;

import StringMap;
import IntMap;

using Traits.MapAccessSyntax;
using Traits.FunctorSyntax;
using Traits.ShallowCopySyntax;

class Main {
	static function main () {

		var map = new IntMap();

		var map2 = map.shallowCopy(_);
		map.set(1, "hey", _);
		trace(map.get(1, _));
		trace(map.map( x -> x.toUpperCase(), _));
		trace(map2);

		var map = new StringMap();
		var map2 = map.shallowCopy(_);
		map.set("a", "hey", _);
		trace(map.get("a", _));
		trace(map.map( x -> x.toUpperCase(), _));
		trace(map2);

		var map = new StringMap();
		map.set("a", "hey", _);
		var map2 = map.shallowCopy(_);

	}
}