import scuts.implicit.Instances;
import Traits;
import scuts.implicit.Mono;
import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import Traits;

import StringMap.StringMapAsFunctor.instance;
import StringMap.StringMapAsMapAccess.instance;

using Traits.FunctorSyntax;
using Traits.MapAccessSyntax;

abstract StringMap<X>(Map<String, X>) {
	public inline function new () {
		this = new Map();
	}

	public inline function raw () return this;
}

@:publicFields
class StringMapAsFunctor implements Functor<StringMap<_>> {

	@:implicit static var instance:Instances<Functor<StringMap<_>>> = new StringMapAsFunctor();

	private function new () {}

	inline function map <A,B> (m:StringMap<A>, f:A->B):StringMap<B> {
		var res = new StringMap();
		var raw = m.raw();
		for (k in raw.keys()) {
			res.set(k, f(raw[k]), _);
		}
		return res;
	}
}

@:publicFields
class StringMapAsMapAccess implements MapAccess<StringMap<_>, String> {

	@:implicit static var instance:Instances<MapAccess<StringMap<_>, String>> = new StringMapAsMapAccess();

	private function new () {}

	inline function get <V>(m:StringMap<V>, key:String):V {
		return m.raw().get(key);
	}

	inline function set <V>(m:StringMap<V>, key:String, v:V):Void {
		m.raw().set(key, v);
	}
}


@:publicFields
class StringMapAsShallowCopy<X> implements ShallowCopy<StringMap<X>> {

	@:implicit static var instance:Instances<ShallowCopy<StringMap<Mono>>> = new StringMapAsShallowCopy();

	private function new () {}

	inline function shallowCopy (t:StringMap<X>):StringMap<X> {
		return t.map(x -> x, _); // uses functor map here
	}
}
