import scuts.implicit.Instances;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import Traits;

import scuts.implicit.Mono;

import IntMap.IntMapAsFunctor.instance;
import IntMap.IntMapAsMapAccess.instance;

using Traits.FunctorSyntax;
using Traits.MapAccessSyntax;

abstract IntMap<X>(Map<Int, X>) {
	public inline function new () {
		this = new Map();
	}
	public inline function raw () return this;
}

@:publicFields
class IntMapAsShallowCopy<X> implements ShallowCopy<IntMap<X>> {

	@:implicit static var instance:Instances<ShallowCopy<IntMap<Mono>>> = new IntMapAsShallowCopy();

	private function new () {}

	inline function shallowCopy (t:IntMap<X>):IntMap<X> {
		return t.map(x -> x, _);
	}
}


@:publicFields
class IntMapAsFunctor implements Functor<IntMap<_>> {

	@:implicit static var instance:Instances<Functor<IntMap<_>>> = new IntMapAsFunctor();

	private function new () {}

	inline function map <A,B> (m:IntMap<A>, f:A->B):IntMap<B> {
		var res = new IntMap();
		var raw = m.raw();
		for (k in raw.keys()) {
			res.set(k, f(raw[k]), _);
		}
		return res;
	}
}

@:publicFields
class IntMapAsMapAccess implements MapAccess<IntMap<_>, Int> {

	@:implicit static var instance:Instances<MapAccess<IntMap<_>, Int>> = new IntMapAsMapAccess();

	private function new () {}

	inline function get <V>(m:IntMap<V>, key:Int):V {
		return m.raw().get(key);
	}

	inline function set <V>(m:IntMap<V>, key:Int, v:V):Void {
		m.raw().set(key, v);
	}

}