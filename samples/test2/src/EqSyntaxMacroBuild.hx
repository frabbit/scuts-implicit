import haxe.macro.Expr;

class EqSyntaxMacroBuild {
	public static function build () {
		var f = macro class X {
			macro public static function eq (a:haxe.macro.Expr, b:haxe.macro.Expr):Expr {
				var f = macro {
					function f <T>(a:T, b:T, c:Eq<T>) {};
					scuts.implicit.macros.Helper.first(f.bind($a,$b));
				}
				var eq = scuts.implicit.macros.Resolver.resolveExpr(f);
				return macro $eq.eq($a,$b);
			}
		}
		return f.fields;
	}

}