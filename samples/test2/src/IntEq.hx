import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;

@:generic class IntEq
	implements Eq<Int> implements scuts.implicit.Trait
{


	public function new () {}
	public function eq (a:Int, b:Int):Bool {
		return a == b;
	}
}