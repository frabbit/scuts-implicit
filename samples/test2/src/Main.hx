package;

import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;

//import StringInstances;

import OrdString.instance;
import OrdInt.instance;
//import ArrayEq.instance;
import ArrayOrd.instance;

//import ArrayEqInstances.instance;
//import IntEqInstances.instance;

//import Plus1.instance;
//import Plus2.instance;
import Plus3.instance;
//import Plus4.instance;
//import Plus5.instance;
//import ArrayEq;
//import ArrayOrd;


//import EqSyntaxMacro;
//import EqSyntaxMacro2;
// for nicer syntax
using Eq.EqSyntax;
using Ord.OrdSyntax;



/*
class FloatInstances
	implements Eq<Float>
{
	@:implicit public static var instance:Eq<Float> = new FloatInstances();

	public function new () {}
	public inline function eq (a:Float, b:Float):Bool {
		return a == b;
	}
}




class BoolInstances
	implements Eq<Bool>
	implements Ord<Bool>
{
	@:implicit public static var instance:Eq<Bool> = new BoolInstances();

	public function new () {}
	public inline function eq (a:Bool, b:Bool):Bool {
		return a == b;
	}
	public inline function compare (a:Bool, b:Bool):Int {
		return switch [a,b] {
			case [true, false]: 1;
			case [false, true]: -1;
			case _: 0;
		}
	}
}
*/
#if !macro
class Main {
	static function main () {
		//trace(Plus.PlusApi.plus(1, "hi", _));

		//trace(Plus.PlusApi.plus(1, 1.1, _));
		//trace(Plus.PlusApi.plus(1.2, 1, _));
		//trace((_:Implicit<Plus<Int, String, Mono>>).plus(1, "hi"));
		//EqIntApi.eq(a,b);
		//var a:EqS<Int> = EqSInt;
		//trace(2.eq(1,_));
		//trace("2".eq("1",_));
		//trace([2].eq([1],_));
		//trace(["3"].eq(["3"],_));
		//trace("3".compare("2", _));
		//trace([3].compare([2], _));
		//trace([[3]].compare([[2]], _));
		//trace(["3"].compare(["2"], _));
		trace(["3"].eq(["2"], _));
		//trace(["3"].compare(["2"], _));

		//trace("3".compare("2", _));


		//instance <T>(o:Ord<T>):ArrayOrd<T> return new ArrayOrd(o);


		//trace(Plus.PlusApi.plus(1, 1, _));
	}
}
#end