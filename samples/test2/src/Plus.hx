import scuts.implicit.Implicit;

interface Plus<A,B, @:open C> {
	public function plus (a:A, b:B):C;
}

class PlusApi {
	public static inline function plus <A,B,C>(a:A, b:B, p:Implicit<Plus<A,B,C>>):C {
		return p.plus(a,b);
	}
}