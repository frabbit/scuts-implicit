import scuts.implicit.Open;

class Plus4 implements Plus<Int, Float, Float> {
	@:implicit public static var instance = new Plus4();
	public function new () {}
	public function plus (a:Int, b:Float):Float {
		return a+b;
	}
}