import scuts.implicit.Implicit;

interface Ord<T> extends Eq<T> {
	public function compare (a:T, b:T):Int;
}

class OrdSyntax {
	public static function compare <T>(a:T, b:T, o:Implicit<Ord<T>>):Int {
		return o.compare(a,b);
	}
}