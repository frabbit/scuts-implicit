#if macro
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.Context;
import haxe.macro.TypeTools;
import haxe.macro.ExprTools;
#end

class BuildEq {
	static var id = 1;
	public static function build () {
		var cmp:Expr = null;
		var ct:ComplexType;
		switch Context.getLocalType() {
			case TInst(_, [t1, TInst(_.get() => { kind : KExpr(e)}, [])]):
				trace(t1);
				ct = TypeTools.toComplexType(t1);
				switch e {
					case macro [{ compare: $cmp1 }]:
						//trace(cmp1);
						cmp = cmp1;
					case _ :
						Context.error("expr expected", Context.currentPos());
				}

			case t:
				Context.error("Class expected", Context.currentPos());
		}


		var name = "EqStatic"+id;


		var tp = { name : name, pack: []};
		var c = macro class $name implements Eq<$ct> {

			public function new () {}

			public static function mk () return new $tp();

			public function eq (a:$ct, b:$ct) {
				return $cmp;
			}
		};

		Context.defineType(c);
		var ct = ComplexType.TPath({name : name, pack : []});
		// hack, because i can't access the constructor without this
		Context.typeof( macro (null:$ct) );

		return ct;
	}
}