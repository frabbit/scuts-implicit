import scuts.implicit.Open;

class Plus2 implements Plus<Int, String, Float> {
	@:implicit public static var instance = new Plus2();
	public function new () {}
	public function plus (a:Int, b:String):Float {
		return 1.1;
	}
}