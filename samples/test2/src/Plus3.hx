import scuts.implicit.Open;

class Plus3 implements Plus<Int, Int, Int> {
	@:implicit public static var instance = new Plus3();
	public function new () {}
	public inline function plus (a:Int, b:Int):Int {
		return a+b;
	}
}