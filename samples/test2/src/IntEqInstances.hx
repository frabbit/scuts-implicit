import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;

class IntEqInstances
	implements scuts.implicit.Trait
{
	@:implicit public static function instance ():Instances<Eq<Int>, IntEq> return new IntEq();

}