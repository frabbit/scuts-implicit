import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;

using Eq.EqSyntax;



class ArrayEq<T> implements Eq<Array<T>>
{

	@:implicit var eqT:Eq<T>;

	public function new (eqT:Eq<T>) {
		this.eqT = eqT;
	}

	public inline function eq (a:Array<T>, b:Array<T>):Bool {
		if (a.length != b.length) return false;

		for (i in 0...a.length) {
			if (!a[i].eq(b[i], _)) return false;
		}
		return true;
	}
}