
class OrdInt extends EqInt implements Ord<Int> {

	@:implicit public static var instance = new OrdInt();

	public function new () {
		super();
	}

	public function compare (a:Int, b:Int):Int {
		return a - b;
	}
}