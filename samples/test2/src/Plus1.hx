
class Plus1 implements Plus<Int, String, Bool> {
	@:implicit public static var instance = new Plus1();
	public function new () {}
	public function plus (a:Int, b:String):Bool {
		return true;
	}
}