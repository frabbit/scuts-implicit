import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;

using Ord.OrdSyntax;

class ArrayOrd<T> extends ArrayEq<T>
	implements Ord<Array<T>>
	implements Eq<Array<T>>
{
	@:implicit public static function instance <T>(o:Ord<T>):ArrayOrd<T> return new ArrayOrd(o);

	@:implicit var ordT:Ord<T>;

	public function new (ordT:Ord<T>) {
		super(ordT);
		this.ordT = ordT;
	}


	public inline function compare (a:Array<T>, b:Array<T>):Int {

		var min = a.length > b.length ? b.length : a.length;

		for (i in 0...min) {
			var c = a[i].compare(b[i], _);
			if (c != 0) return c;
		}

		return if (a.length < b.length) {
			-1;
		} else if (a.length > b.length) {
			1;
		} else {
			0;
		}
	}
}