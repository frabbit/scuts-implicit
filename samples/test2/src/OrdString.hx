
class OrdString extends EqString implements Ord<String> {

	@:implicit public static var instance = new OrdString();

	public function new () {
		super();
	}

	public function compare (a:String, b:String):Int {
		return a < b ? -1 : a > b ? 1 : 0;
	}
}