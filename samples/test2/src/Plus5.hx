import scuts.implicit.Open;

class Plus5 implements Plus<Float, Int, Float> {

	@:implicit public static var instance = new Plus5();
	public function new () {}
	public function plus (a:Float, b:Int):Float {
		return a+b;
	}
}