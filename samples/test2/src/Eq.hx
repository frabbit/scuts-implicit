import scuts.implicit.Implicit;

interface Eq<T> {
	public function eq (a:T, b:T):Bool;
}

class EqSyntax {
	@:generic public static function eq <T>(a:T, b:T, eq:Implicit<Eq<T>>):Bool {
		return eq.eq(a,b);
	}
}

/*
class Eq<T> {
	public static function eq (a:T, b:T):Bool;
}

typedef Eq = {
	function eq <T>(a:T, b:T):Bool;
}

class Eq {
	public static function eq <T>(a:T, b:T):Bool;
}

class Eq {
	public static function eq (a:Int, b:Int):Bool;
}


*/