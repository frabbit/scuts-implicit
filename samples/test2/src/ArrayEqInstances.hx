import scuts.implicit.Wildcard;
import scuts.implicit.Implicit;
import scuts.implicit.Instances;
import scuts.implicit.InstancesAs;
import scuts.implicit.Mono;
import ArrayEq;
import Eq;

class ArrayEqInstances implements scuts.implicit.Trait {
	@:implicit @:generic public static function instance <T>(eq:Eq<T>) return new ArrayEq(eq);
}