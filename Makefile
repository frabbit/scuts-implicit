.PHONY: test dump

test:
	haxe test.hxml -neko bin/test.n --times -D macro_times -D implicits_show_resolved=all
	neko bin/test.n

test-java:
	haxe test.hxml -java bin/java --times -D macro_times -D erase-generics
	java -jar bin/java/TestAll.jar

test-cpp:
	haxe test.hxml -cpp bin/cpp --times -D macro_times
	bin/cpp/ImplicitScopeTests

test-hl:
	haxe test.hxml -hl bin/hl.c --times -D macro_times
	gcc -o bin/hl-app hlruntime.a bin/hl.c

test-cs:
	haxe test.hxml -cs bin/cs --times -D macro_times -D erase_generics
	bin/cs/bin/TestAll.exe

test-py:
	haxe test.hxml -python bin/test.py --times -D macro_times
	python3 bin/test.py

test-lua:
	haxe test.hxml -lua bin/test.lua --times -D macro_times
	lua bin/test.lua

test-swf:
	haxe test.hxml -swf bin/test.swf --times -D macro_times
	flashplayerdebugger bin/test.swf

test-js:
	haxe test.hxml -js bin/test.js -D nodejs --times -D macro_times
	node bin/test.js

dump:
	haxe test.hxml -neko bin/test.n --times -D macro_times -D dump=pretty



issue1:
	haxe issue.hxml -neko bin/issue1.n -cp issues/issue1 -main Main --times -D macro_times -D implicits_show_resolved=all
	neko bin/issue1.n