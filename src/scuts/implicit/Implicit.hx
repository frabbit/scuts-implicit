package scuts.implicit;

#if (macro && (eval || neko))
import haxe.macro.Context;
import scuts.implicit.macros.Resolver;
import scuts.implicit.macros.Typer;
import scuts.implicit.macros.FastContext;
import haxe.macro.Expr;
import haxe.macro.ExprTools;
#end

@:forward
@:callable
@:arrayAccess
@:extern abstract Implicit<T>(T) from T to T {

  inline function new (x:T) this = x;
  @:noUsing @:extern public static inline function mk <T>(x:T):Implicit<T> return new Implicit(x);


  @:from
  macro public static function fromExpr (e:haxe.macro.Expr) {
    //trace("called");
    //trace("call me");
    var type = FastContext.typeofFast(e);
    var isWildcard = switch FastContext.followFast(type) {
      case TEnum(t, []) if (t.toString() == "scuts.implicit.Wildcard"):
        true;
      case t :
        false;
    }

    if (!isWildcard) {
      return e;
      //return macro scuts.implicit.Implicit.mk($e);
    }

    var expected = FastContext.getExpectedTypeFast();

    var expectedType = switch expected {
      case TAbstract(_.toString() => "scuts.implicit.Implicit", [t]):
        t;
      case t:
        trace(t);
        throw ("error:" + Std.string(t));
    }
    //trace(expectedType);
    var getType = () -> {
      return Typer.duplicateType(expectedType);
    }

	  var res = Resolver.resolveByType(getType(), getType);
    var x = macro $res;

    return x;

  }



  /**
   * Returns an implicit Object based on the type passed as a string.
   *
   * Usage: Implicit.implicitByType("scuts.ht.classes.Monad<Array<_>>");
   */
  @:noUsing
  macro public static function implicitByType (type:String):Expr
  {
    return Resolver.resolveByTypeString(type);
  }

}