enum PluginStage {
	BeforeAll;
	BeforeMembers;
	BeforeStatics;
	BeforeExplicitImports;
	BeforeUsing;
	BeforeWildcardImport;
	BeforeImport;
	AfterAll;
}