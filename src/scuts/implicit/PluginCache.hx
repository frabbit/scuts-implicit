package scuts.implicit;

#if macro
import scuts.implicit.macros.Cache;
import haxe.ds.Option;
import haxe.macro.Expr;

class PluginCache {

	static var nextId:Int = 0;
	static var caches:Map<Int, Cache<Option<Expr>>> = new Map();
	public static function get (enabled:Bool=true) {

		var id = nextId++;
		var c = caches.get(id);
		if (c == null) {
			c = new Cache(enabled);
			caches.set(id, c);
		}
		return c;
	}


	static var init = {
		haxe.macro.Context.onMacroContextReused( () -> {
			for (c in caches) {
				c.clear();
			}
			return true;
		});
	}

}
#end