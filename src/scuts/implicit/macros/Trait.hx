package scuts.implicit.macros;

#if macro
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.Context;
import haxe.macro.TypeTools;
import haxe.macro.ExprTools;
import haxe.ds.Option;
#end


class Trait {
	public static function build () {
		var c = Context.getLocalClass().get();
    	if (c.meta.has(":traitProcessed")) return null;
    	c.meta.add(":traitProcessed",[],c.pos);


		//trace(c.superClass);

		//trace("build trait");
		var fields = Context.getBuildFields();



		var newFields = fields.copy();
		/*switch (getConstructor(fields)) {
			case Some(x) if (!hasSuper(c)):

				var other = macro class {
					@:implicit public static function instance <T>(eq:Eq<T>):Instances<Eq<Array<T>>, Eq<Array<T>>> return new ArrayEq(eq);
				};
				newFields.push(other.fields[0]);
			case _:
				trace("None");
		}*/
		switch getInstance(newFields) {
			case Some(f):

				var isGeneric = isGeneric(f);

				switch f.kind {
					case FFun(fun):
						//trace(fun.ret);
						switch (fun.ret) {
							case TPath({ name : "Instances", params: params}):
								var id = 0;
								var names = [];
								var allNames = [];

								var last = params[params.length-1];

								for (i in 0...params.length-1) {
									var p = params[i];
									var name = f.name + id;
									switch p {
										case TPType(ct):
											var fun = copyFieldKindFun(fun, ct);
											var field = copyFieldWithNameAndFunction(f, name+"Typer", fun);

											newFields.push(field);

											if (isGeneric) {
												if (field.meta == null) {
													field.meta = [];
												}
												field.meta.push({ name : ":extern", pos : field.pos});
												var f = copyFieldWithGeneric(f);
												//trace("push generic");
												newFields.push(f);

											}

										case _:
									}
									allNames.push(macro $i{name});
									id++;
								}
								switch last {
									case TPType(ct):
										fun.ret = ct;
									case _:
										throw "error";
								};
								f.meta.push({ name : ":implicitInstances", params: allNames, pos : f.pos});
								//trace(f.meta);
							case _:
								trace("no return");
						}
					case _:
						trace("no func instance");
				}

			case None:
		}


		return newFields;
	}

	static function isGeneric (f:Field) {
		var ret = false;

		for (m in f.meta) {
			//trace(m.name);
			if (m.name == ":generic") {
				ret = true;
				break;
			}
		}
		return ret;
	}
	static function copyFieldKindFun (f:Function, ct:ComplexType) {
		return {
			args: f.args,
			expr: f.expr,
			params: f.params,
			ret : ct
		}
	}
	static function copyFieldWithNameAndFunction (field:Field, name:String, f:Function) {

		return {
			access: field.access,
			doc: field.doc,
			kind: FFun(f),
			meta: null,
			name: name,
			pos: field.pos,
		}
	}

	static function copyFieldWithGeneric (field:Field) {
		//trace(field);
		//trace(field.meta);
		var meta = field.meta != null ? field.meta.copy() : [];
		meta.push({ name : ":generic", pos : field.pos});
		//meta.push({ name : ":keep", pos : field.pos});
		return {
			access: field.access,
			doc: field.doc,
			kind: field.kind,
			meta: meta,
			name: field.name + "Generic",
			pos: field.pos,
		}
	}

	static function getConstructor (fields:Array<Field>) {
		var found = None;
		for (f in fields) {
			if (f.name == "new") {
				found = Some(f);
				break;
			}
		}
		return found;
	}

	static function getInstance (fields:Array<Field>) {
		var found = None;
		for (f in fields) {
			if (f.name == "instance") {
				found = Some(f);
				break;
			}
		}
		return found;
	}

	static function hasSuper (cl:ClassType) {
		return cl.superClass != null;
	}
}