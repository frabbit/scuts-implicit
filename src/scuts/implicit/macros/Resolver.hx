package scuts.implicit.macros;

#if (macro && (eval || neko))
import haxe.ds.StringMap;

import haxe.macro.TypeTools;

import scuts.implicit.macros.Cache;
import scuts.implicit.macros.Errors;
import scuts.implicit.macros.Typer;

import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.Type;
import haxe.macro.ExprTools;

import scuts.implicit.macros.Data;

using scuts.implicit.macros.Arrays;
using scuts.implicit.macros.Validations;
using scuts.implicit.macros.Options;

using scuts.implicit.macros.Data.ResolveResults;



class Resolver
{
	static var isShowResolved = Context.defined("implicits_show_resolved");
	static var isShowAllResolved = Context.definedValue("implicits_show_resolved") == "all";
	static var isDisplayMode = false; // Context.defined("display");
	/**
	 * Returns an Expression representing the implicit Object for the given type t.
	 * The type is encoded as a string, because something like Array<Int> is not a valid expression.
	 */
	public static function resolveByTypeString (t:String):Expr
	{
		if (isDisplayMode) return displayResolveByTypeString(t);

		var getRequired = () -> {
			Typer.preTypeExpr(Context.parse(" { var e : " + t + " = null; e; } ", Context.currentPos()));
		}
		return resolveByTypeHelper( getRequired, getRequired, None);
	}

	public static function resolveFirstFunctionArgument (fun:Expr):Expr
	{
		var required = macro scuts.implicit.macros.Helper.first($fun);
		return resolveExpr(required);
	}

	public static function resolveExpr (e:Expr):Expr
	{
		var getRequired = () -> Typer.preTypeExpr(e);

		return resolveByTypeHelper(getRequired, getRequired, None);
	}

	public static inline function mkExprWithType (t:Type) {
		var texpr = { expr: TConst(TNull) , pos: FastContext.currentPosFast(), t: t};
		return FastContext.storeTypedExprFast(texpr);
	}

	public static function resolveByType (t:haxe.macro.Type, cleanType:Void->haxe.macro.Type):Expr
	{
		if (isDisplayMode) return displayResolveByType(t);

		var getRequired = () -> mkExprWithType(t);
		var getRequiredClean = () -> mkExprWithType(cleanType());

		return resolveByTypeHelper( getRequired, getRequiredClean, Some(t));
	}

	/*---------------------INTERNAL--------------*/

	static function resolveByTypeHelper (getRequired : Void -> Expr, getCleanRequired:Void->Expr, type:Option<Type>)
	{
		//trace("--------------------------------------start resolve---------------");
		//trace(Typer.typeof(getRequired()));
		//trace(haxe.macro.ExprTools.toString(getRequired()));
		//trace(haxe.macro.ExprTools.toString(getCleanRequired()));


		return try
		{
			var ctx = ResolveContexts.create(getRequired, getCleanRequired, type);
			#if implicitDebug
			//trace("pre ids");
			trace(ctx.ids());
			#end
			switch (resolveContext(ctx)) {
				case RRSuccess(e,c):
					if (isShowResolved) {
						var pos = Context.currentPos();
						haxe.macro.Context.warning(Candidates.getColoredNameWithType(c), pos);
					}
					e;
				case RRNotFound:
					//trace(haxe.macro.ExprTools.toString(getRequired()));
					//trace(haxe.macro.Context.typeof(getRequired()));
					//trace("not found");
					resolverError(NoImplicitObjectInContext(getRequired()));
				case RRFailure(f): resolverError(f);
			}
		}
		catch (e:ResolverError) {
			trace(e);
			resolverError(e);
		}
		catch (e:haxe.macro.Error) {
			trace(haxe.CallStack.callStack());
			trace(getRequired());
			Context.error(Std.string(e), Context.currentPos());
			trace(std.Type.typeof(e));
			trace(e);
			throw "unexpected";
		}
	}

	static function displayResolveByTypeString (t:String):Expr
	{
		return Context.parse(" { var e : " + t + " = null; e; } ", Context.currentPos());
	}

	inline static function displayResolveByType (t:Type) {
		var ct = TypeTools.toComplexType(t);
		return macro @:pos(Context.currentPos()) { var e : $ct = throw "error"; e; };
	}
	static function resolverError (err:ResolverError) {
		return Context.error(ErrorPrinter.toString(err), Context.currentPos());
	}

	/**
	 * Contains the actual loop over all parameters.
	 */
	static function resolveFunctionArgs (functionExpr:Expr, cleanFunctionExpr:Void->Expr, rawFuncExpr:Expr, numParams:Int,
		ctx:ResolveContext, newStack:ResolveStack):Validation<ResolverError, Array<Expr>>
	{
		//trace("resolve function args");
		function f(acc:Validation<ResolverError, Array<Expr>>, i:Int)
		{
			return acc.flatMap(newArgs -> {
				// resolve the argument, but don't resolve in display mode
				return if (isDisplayMode)
				{
					Success(newArgs.concat([macro throw "error"]));
				}
				else {
					var ctx = ResolveContexts.createForFunctionArg(functionExpr, cleanFunctionExpr, rawFuncExpr, numParams, ctx, newStack, newArgs, i);

					switch resolveContext(ctx) {
						case RRSuccess(e,c):
							if (isShowAllResolved) {
								var prefix = "";
								for (_ in 0...ctx.stack.length) {
									prefix += "    ";
								}
								haxe.macro.Context.warning(prefix + Candidates.getColoredNameWithType(c), Context.currentPos());
							}

							Success(newArgs.concat([e]));
						case RRFailure(f): Failure(f);
						case RRNotFound: Failure(NoImplicitObjectInContext(ctx.required));
					};
				}
			});
		}

		var indices = [for (i in 0...numParams) i];
		return indices.foldLeft(Success([]), f);
	}



	static function checkForCircularDependency (ctx:ResolveContext):CheckCircularDependencyResult
	{
		if (ctx.stack.length == 0) return NoCircularDependency;
		var hasLazy = false;
		for (i in 0...ctx.stack.length) {
			var j = ctx.stack.length - 1 - i; // from tail to head
			var cur = ctx.stack[j];
			if (ctx.id() == cur.id() && Typer.sameTypeByImplicit(ctx.requiredImplicit, cur.requiredImplicit)) {
				//trace(hasLazy);
				if (hasLazy) {
					cur.ctx.lazyUsed = true;
					return LazyExpression( macro $i{"__resolved"+cur.ctx.depth});
				} else {
					return Error(CircularDependency(cur, ctx));
				}

			}
			hasLazy = hasLazy || cur.ctx.isLazy;
		}
		return NoCircularDependency;
		/*return ctx.stack
			.some( si -> ctx.id() == si.id() && Typer.sameTypeByImplicit(ctx.requiredImplicit, si.requiredImplicit))
			.map(CircularDependency.bind(_, ctx));*/
	}

	static function getImplicitIfCompatible (candidate:Candidate, ctx:ResolveContext)
	{
		var cur = candidate.exprImplicit();

		//trace(Typer.typeof(ctx.requiredAsParam));


		//// TODO unify with mono sideeffects!!!!!




		//trace(Typer.typeof(candidate.safeExpr()));
		//trace(Typer.typeof(candidate.genExpr()));


		//trace(Typer.typeof(cur));
		//trace(Typer.typeof(ctx.requiredAsParam));
		var res = if (Typer.isCompatibleWithTypeAsParamExpr( cur, ctx.requiredAsParam)) {

			Some(makeResultExpr(ctx, candidate.genExpr()));
		}else Option.None;

		//trace(res);

		return res;

	}

	static function makeResultExpr (ctx:ResolveContext, expr:Expr) {
		var wrapLazy = (e:Expr) -> {
			if (ctx.isLazy) {
				macro {
					var set = false;
					var val;
					(?_) -> {
						if (!set) {
							val = $expr;
							set = true;
						}
						val;
					}
				}
			} else {
				expr;
			}
		}
		var expr = wrapLazy(expr);
		var x = if (ctx.lazyUsed) {
			var uniqueName = "__resolved" + ctx.depth;
			macro { var $uniqueName = null; $i{uniqueName} = $expr; $i{uniqueName};};
		} else {
			expr;
		}
		//trace(ExprTools.toString(x));
		return x;
	}

	static function resolveFunction (candidate:Candidate, args, ctx:ResolveContext)
	{
		//trace("resolve function");
		var rawFuncExpr = candidate.safeExpr();

		return switch args.length {
			case 0:
				var genExpr = candidate.genExpr();
				// function requires no arguments, just call it
				Success(makeResultExpr(ctx, macro $genExpr()));
			case _:
				//trace(rawFuncExpr);
				// function requires arguments, but these arguments must be compatible to the required type,
				// which could be more specific than the raw return type (e.g. type parameters).

				var specializedFuncExpr = () -> Typer.preTypeExpr(Typer.specializeFunctionExpr(rawFuncExpr, ctx.required, args.length));
				var newStack:ResolveStack = ctx.stack.concat([{ id: ctx.id, required : ctx.required, candidate : candidate , requiredImplicit: ctx.requiredImplicit, ctx: ctx }]);

				var rawFuncExpr = candidate.genExpr();
				resolveFunctionArgs(specializedFuncExpr(), specializedFuncExpr, rawFuncExpr, args.length, ctx, newStack).map( res -> {

					makeResultExpr(ctx, macro @:pos(rawFuncExpr.pos) $rawFuncExpr($a{res}));
				});
		}
	}

	static function checkCandidate (candidate:Candidate, ctx:ResolveContext)
	{
		//trace("check candidate");
		return switch [candidate.typeFollowed(), candidate.retImplicit()]
		{
			case [TInst(_,_), _]:
				//trace("here");
				getImplicitIfCompatible(candidate, ctx).map(Success);
			case [TAbstract(_,_), _]:
				getImplicitIfCompatible(candidate, ctx).map(Success);
			case [TEnum(_,_), _]:
				getImplicitIfCompatible(candidate, ctx).map(Success);
			case [TAnonymous(_),_]:
				getImplicitIfCompatible(candidate, ctx).map(Success);
			case [TFun(args, _), Some(retImplicit)] if (Typer.isCompatibleWithTypeAsParamExpr(retImplicit, ctx.requiredAsParam)):
				// now we need to type all function arguments recursively
				//trace("try function resolve");
				Some(resolveFunction(candidate, args, ctx));
			case [TFun(_,_), _] if (Typer.isCompatibleWithTypeAsParamExpr(candidate.exprImplicit(), ctx.requiredAsParam)):
				//trace("fallback here");
				getImplicitIfCompatible(candidate, ctx).map(Success);
			case _:
				//trace("none found");
				Option.None;
		}
	}



	/**
	 * Internal function for the resolution of implicit objects.
	 */
	static function resolveContext (ctx:ResolveContext):ResolveResult
	{

		inline function filterAndFindCandidate (ctx, candidateMap:Map<String, Array<Void -> Candidate>>, scope:Scope) {
			//trace("prefilter");
			//for (k in candidateMap.keys()) {
			//	trace(k);
			//	//trace(candidateMap[k].map(Data.Candidates.getColoredNameWithType));
			//}

			//trace(ctx.ids());

			var candidates = Collector.preFilterCandidates(ctx.ids, candidateMap);
			//trace("prefilter after");
			//trace(candidates.map(Data.Candidates.getColoredNameWithType));
			return findCandidate(ctx, candidates, scope);
		}
		function f ()
		{
			var s = ctx.scopes;
			var res = RRNotFound;

			for (curScope in State.instance.allScopes) {
				res = switch (curScope) {
					case Plugin(stage):
						var plugins = State.getPluginArray(stage);
						var res = RRNotFound;
						for (p in plugins) {
							switch p(ctx) {
								case Some(e):
									var candidate = Collector.createCandidate(e, FastContext.typeofFast(e), PluginInfo);
									res = findCandidate(ctx, [candidate], Plugin(stage));
								case None:
							}
							if (!res.match(RRNotFound)) break;
						}
						res;
					case Local:
						findCandidate(ctx, s.locals(), curScope);
					case Member:
						findCandidate(ctx, s.members(), curScope);
					case Static:
						findCandidate(ctx, s.statics(), curScope);
					case ExplicitImport:
						//trace("EXPLICIT");
						filterAndFindCandidate(ctx, s.importExplicitMap(), ExplicitImport);
					case Using:
						filterAndFindCandidate(ctx, s.usingMap(), Using);
					case Import:
						filterAndFindCandidate(ctx, s.importMap(), Import);
					case WildcardImport:
						filterAndFindCandidate(ctx, s.importWildcardMap(), WildcardImport);
				}
				if (!res.match(RRNotFound))
					break; // ugly but more efficient
			}
			return res;
		}
		// check for circular dependency first, then check locals + class content, then outer context.
		return switch checkForCircularDependency(ctx) {
			case Error(x): RRFailure(x);
			case LazyExpression(e): RRSuccess(e, null);
			case NoCircularDependency: f();
		}
	}



	static inline function findCandidate (ctx:ResolveContext, candidates:Array<Void->Candidate>, currentScope:Scope):ResolveResult
	{
		function f () {
			var successes = [];
			var failures = [];
			//trace(candidates);

			var monosBegin = switch Typer.typeof(ctx.requiredAsParam) {
				case Some(t):
					Tools.countMonos(t);
				case None:
					0;
			}
			//trace("check2");
			//var dup = Typer.duplicateType(Context.getExpectedType());

			var run = 0;
			for (c in candidates) {
				//trace("loop" + run);
				var ctx = if (run > 0) {
					ResolveContexts.restoreMonos(ctx);
				} else ctx;
				var monosCurrent = switch Typer.typeof(ctx.requiredAsParam) {
					case Some(t):
						Tools.countMonos(t);
					case None:
						0;
				}

				if (monosCurrent != monosBegin) {

					throw "monos changed" + (monosCurrent + " -> " + monosBegin);
				}
				run++;
				var c = c();
				//trace(Typer.typeof(ctx.required));
				//trace(Typer.typeof(ctx.requiredImplicit));
				//trace(Typer.typeof(ctx.requiredAsParam));
				var check = checkCandidate(c, ctx);
				//trace(Typer.typeof(ctx.required));
				//trace(Typer.typeof(ctx.requiredImplicit));
				//trace(Typer.typeof(ctx.requiredAsParam));
				//trace("success");
				switch (check) {
					case Some(x):
						switch (x) {
							case Success(expr):
								if (candidates.length == 1) { // optimization
									//trace("optimize");
									return RRSuccess(expr, c);
								}
								successes.push({ expr : expr, candidate : c });
							case Failure(f):
								failures.push(f);
						}
					case None:
						//trace("none success");
				}
			}

			return switch (successes)
			{
				case []:
					//trace("no success");
					// if we have failures here, we should propagate them.
					//var failures = options.catFailures();
					switch failures {
						case []: RRNotFound;
						case [f]: RRFailure(f);
						case failures:
							RRFailure(failures[0]);
					}
				case [{ expr : expr, candidate : c }]:
					//trace("candidate found");
					// we habe exactly one candidate, perfect
					RRSuccess(expr, c);
				case results :
					//trace("candidate failed");
					// we have more than one candidate, error ambigouis
					RRFailure(ImplicitAmbiguityError(currentScope, results.map(x -> x.candidate), ctx));
			}
		}
		return if (candidates.length == 0) { // optimization
			RRNotFound;
		} else {
			f();
		}
	}
}
#end