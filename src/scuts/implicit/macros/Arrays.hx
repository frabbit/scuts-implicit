package scuts.implicit.macros;

import scuts.implicit.macros.Validations;

using scuts.implicit.macros.Options;

class Arrays
{

  /**
   * Concatenates all Some X values in a and returns a new list containing only X values.
   */
  public static inline function catOptions <X>(a:Array<Option<X>>):Array<X>
  {
    var res = [];

    for (e in a)
    {
      switch (e) {
        case Some(v): res.push(v);
        case None:
      }
    }
    return res;
  }

  public static function reversed <A> (a:Array<A>):Array<A>
  {
    var c = a.length;
    var res = [];
    for (e in a) {
      res[--c] = e;
    }
    return res;
  }

  public static inline function foldLeft<A,B>(arr:Array<A>, acc:B, f:B->A->B):B
  {
    for (i in 0...arr.length)
    {
      acc = f(acc, arr[i]);
    }
    return acc;
  }

  public static function some <T>(arr:Array<T>, e:T->Bool):Option<T>
  {
    for (i in arr)
    {
      if (e(i)) return Some(i);
    }
    return None;
  }

  public static function any <T>(arr:Array<T>, e:T->Bool):Bool
  {
    for (i in arr)
    {
      if (e(i)) return true;
    }
    return false;
  }

  public static function sum <T>(arr:Array<T>, e:T->Int):Int
  {
    var res = 0;
    for (i in arr)
    {
      res += e(i);
    }
    return res;
  }


}


