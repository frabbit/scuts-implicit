package scuts.implicit.macros;

#if (macro && (eval || neko))

import haxe.macro.Context;

import haxe.macro.Expr;
import scuts.implicit.macros.Data;

import scuts.implicit.macros.Typer;

using scuts.implicit.macros.Arrays;
using scuts.implicit.macros.Options;

/**
 * Error Logging
 */
class ErrorPrinter
{

  public static function toString (e:ResolverError) {
    return switch (e) {
      case NoImplicitObjectInContext (required): noImplicitObjectInContext(required);
      case CircularDependency (stackItem, ctx): circularDependency(stackItem, ctx);
      case FunctionExprIsNotAFunction (functionExpr): functionExprIsNotAFunction(functionExpr);
      case FunctionExprCannotBeTyped (functionExpr): functionExprCannotBeTyped(functionExpr);
      case ImplicitAmbiguityError(scope, candidates, ctx): implicitAmbiguityError(scope, candidates, ctx);
      case UnexpectedError(err): err;
    }
  }

  public static function implicitAmbiguityError(scope:Scope, candidates:Array<Candidate>, ctx:ResolveContext)
  {
    function makeMessage (scopeDesc:String, arr:Array<Candidate>) {

      var e = ctx.required;

      var typeStr = Tools.prettyTypeOfExpr(macro scuts.implicit.macros.Helper.removeImplicit($e));

      var stackString = ctx.stack.length == 0 ? "" :
        "\nError occured while:\n" + Arrays.reversed(ctx.stack).map(function (e) {
            return '- Resolving ${Candidates.getName(e.candidate)}'
            +    '\n  While searching for ${Tools.prettyTypeOfExpr(e.required)}';
        }).join("\n");



      return "Ambiguity Error Multiple Implicit Objects for Type " + typeStr + " in " + scopeDesc + "\n"
          +  "Namely:\n" + arr.map( x -> "    "+Candidates.getName(x)).join("\n")
          +  "\nMake sure that only one of them is present/imported."
          + stackString;

    }
    var scopeStr = switch scope {
      case Local : "Local-Scope";
      case Member : "Member-Scope";
      case Static : "Static-Scope";
      case Using : "Using-Scope";
      case Import : "Import-Scope";
      case Plugin(stage): "Plugin-Scope-" + Std.string(stage);
      case ExplicitImport : "ExplicitImport-Scope (Statics from other Files)";
      case WildcardImport : "WildcardImport-Scope (*)";
    }
    var msg = makeMessage(scopeStr, candidates);
    return msg;
  }

  public static function noImplicitObjectInContext (required:Expr)
  {
    return "Cannot find implicitObj Definition for type " + Typer.typeof(required).map(Tools.prettyType).getOrElseConst("`(Untypable Type)`") + " in current context";
  }
  public static function circularDependency (stackItem:StackItem, ctx:ResolveContext) {
    return
      "Circular dependency\n"
      + "Circular dependency between " + Typer.typeof(ctx.required).map(Tools.prettyType) + " and " + Typer.typeof(stackItem.required).map(Tools.prettyType) + " in current scope\n"
      + "This may happen if the passed expression contains unsafe casts";
  }

  public static function functionExprIsNotAFunction (functionExpr:Expr)
  {
    return "The expression ( " + haxe.macro.ExprTools.toString(functionExpr) + " ) is not function type at " + Std.string(Context.getPosInfos(functionExpr.pos));
  }

  public static function functionExprCannotBeTyped (functionExpr:Expr)
  {
    return "Cannot determine the type of expression ( " + haxe.macro.ExprTools.toString(functionExpr) + " ) at " + Std.string(Context.getPosInfos(functionExpr.pos));
  }


}

#end