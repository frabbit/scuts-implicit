package scuts.implicit.macros;
#if macro

import haxe.ds.StringMap;

class Cache<T> {

	var enabled = true;

	var cache:StringMap<T>;

	public function new (enabled : Bool = true) {
		this.enabled = enabled;

		if (enabled) {
			cache = new StringMap();
		}
	}

	public function clear () {
		cache = new StringMap();
	}

	public inline function set (key:String, val:T):T {
		if (enabled && key != null) {
			cache.set(key, val);
		}
		return val;
	}

	public inline function getOrSet (key:String, f:Void->T):T {
		return if (enabled) {
			var v = cache.get(key);
			if (v != null) {
				v;
			} else {
				var r = f();
				cache.set(key, r);
				r;
			}
		} else {
			f();
		}
	}

	public inline function get (key:String):T {
		return if (enabled) {
			cache.get(key);
		} else {
			throw "cannot get value from disabled cache";
		}
	}

	public inline function exists (key:String) {
		return enabled && cache.exists(key);
	}

}
#end