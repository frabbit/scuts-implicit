package scuts.implicit.macros;

#if (macro && (eval || neko))
import haxe.ds.StringMap;

import haxe.macro.TypeTools;

import scuts.implicit.macros.Cache;
import scuts.implicit.macros.Errors;
import scuts.implicit.macros.Typer;
import scuts.implicit.macros.Data;
import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.Type;



import scuts.implicit.macros.Data;

using scuts.implicit.macros.Arrays;
using scuts.implicit.macros.Options;



class Collector
{



	static inline function getLocalImports () {
		return Context.getLocalImports();
	}

	static inline function getLocalUsing () {
		return Context.getLocalUsing();
	}

	static function findFieldImport (fieldName:String, className:String, moduleName:String, pack:Array<String>):Option<Import>
	{
		var types = getModuleTypes(moduleName, pack);
		for (t in types) {
			switch t {
				case TInst( _.get() => cl, _) if (cl.name == className):
					for (f in cl.statics.get()) {
						if (f.name == fieldName && f.isPublic && f.meta.has(":implicit")) {
							return Some(ImportField(cl, f));
						}
					}
				case _:
			}
		}
		return None;
	}

	static function getModuleTypes (moduleName:String, pack:Array<String>)
	{
		var packStr = pack.join(".");
		var fullModuleName = packStr + (packStr.length == 0 ? "" : ".") + moduleName;

		return Context.getModule(fullModuleName);
	}

	static function findClassType (className:String, moduleName:String, pack:Array<String>):Option<Import>
	{
		var types = getModuleTypes(moduleName, pack);
		for (t in types) {
			switch t {
				case TInst( _.get() => cl, _) if (cl.name == className):
					return Some(ImportWildcard(cl));
				case _:
			}
		}
		return None;
	}



	static function getImports1 () {
		var imports = getLocalImports();
		var importKinds = [];
		for (i in imports) switch (i.mode) {
			case IAll:
				var path = [for (p in i.path) p.name];

				function importMainType () {
					var pack = path.slice(0, -1);
					var className = path[path.length-1];
					switch findClassType(className, className, pack) {
						case Some(k): importKinds.push(k);
						case None:
					}
				}

				if (path.length == 1) {
					// Main Type Import
					importMainType();
				} else {
					var last = path[path.length-1];
					var prev = path[path.length-2];
					if (prev.toLowerCase() != prev) {
						// It's a subtype
						var pack = path.slice(0, -2);

						var moduleName = prev;
						var className = last;
						switch findClassType(className, moduleName, pack) {
							case Some(k):
								importKinds.push(k);
							case None:
						}
					} else {
						//Main Type Import
						importMainType();
					}
				}
			case INormal, IAsName(_):

				var name = [for (p in i.path) p.name].join(".");

				try {
					var types = Context.getModule(name);

					if (types != null) {

						for (t in types) {
							switch t {
								case TInst( cl, _): importKinds.push(ImportClass(cl.get()));
								case _ :
							}
						};
					}
				} catch (e:Dynamic) {
					// maybe it's a subtype
					var path = i.path.slice(0, -1);
					var className = i.path[i.path.length-1].name;

					var moduleName = [for (p in path) p.name].join(".");

					try {
						var types = Context.getModule(moduleName);
						var found = false;
						for (t in types) {
							switch t {
								case TInst( _.get() => cl, _) if (cl.name == className):
									importKinds.push(ImportClass(cl));
									found = true;
									break;
								case _ :
							}
						};
						if (!found) {

							// it's a static of the main module class
							var path = i.path.slice(0, -2);
							var className = i.path[i.path.length-2].name;
							var fieldName = i.path[i.path.length-1].name;
							var pack = [for (p in path) p.name];

							switch findFieldImport(fieldName, className, className, pack) {
								case Some(x): importKinds.push(x);
								case None:
							}
						}

					} catch (e:Dynamic) {
						var path = i.path.slice(0, -3);

						var className = i.path[i.path.length-2].name;
						var moduleName = i.path[i.path.length-3].name;
						var fieldName = i.path[i.path.length-1].name;

						var pack = [for (p in path) p.name];
						switch findFieldImport(fieldName, className, moduleName, pack) {
							case Some(x): importKinds.push(x);
							case None:
						}
					}
				}
		};
		return importKinds;
	}

	public static function getImports (ctx:CollectorContext):Array<Import> {
		return switch ctx.localClass() {
			case Some({ id: id}):
				State.instance.importCache.getOrSet(id, getImports1);
			case None:
				getImports1();
		}
	}



	public static inline function getImportTypes (ctx:CollectorContext):Array<ClassType> {
		inline function f (ctx) {
			return getImports(ctx).map( i -> switch i {
				case ImportClass(ct): Some(ct);
				case _ : None;
			}).catOptions();
		}
		return switch ctx.localClass() {
			case Some({ id: id}):
				State.instance.getImportTypesCache.getOrSet(id, () -> f(ctx));
			case None:
				f(ctx);
		}
	}


	public static function getImportWildcardTypes (ctx:CollectorContext):Array<ClassType> {
		inline function f (ctx) {
			return getImports(ctx).map( i -> switch i {
				case ImportWildcard(ct): Some(ct);
				case _ : None;
			}).catOptions();
		}
		return switch ctx.localClass() {
			case Some({id:id}):
				State.instance.getImportWildcardTypesCache.getOrSet(id, () -> f(ctx));
			case None:
				f(ctx);
		}


	}

	public static function addCandidatesFromField(
		expr:Expr,
		baseType:Type,
		res:Map<String, Array<Void->Candidate>>,
		info:Void->CandidateInfo,
		defHash:Void->String)
	{
		//trace("add candidate");
		function addCandidate (type:Void->Type, genExpr:Expr, typeExpr:Void->Expr, hash:Void->String) {
			//trace("pre hash");
			var h = hash();
			//trace("post hash");
			var c = createCandidate1(genExpr, typeExpr, type, info());
			if (res.exists(h)) res.get(h).push(c)
			else res.set(h, [c]);
		}


		var fieldType = Context.follow(baseType);
		var retType = switch fieldType {
			case TFun(_, ret): ret;
			case t: t;
		}
		//trace("add candidate1");
		switch Context.follow(retType) {
			case TInst(_.get() => cl, p) if (!cl.isInterface):

				for ( i in cl.interfaces) {
					var iType1 = TInst(i.t, i.params);

					var iType1 = TypeTools.applyTypeParameters(iType1, cl.params, p);

					var iType = switch fieldType {
						case TFun(args, _):
							TFun(args, iType1);
						case _: iType1;
					}

					var typeExpr = () -> Resolver.mkExprWithType(Typer.addMonos(iType));
					addCandidate( () -> iType, expr, typeExpr, mkTypeHash.bind(iType1));
				}
			case _:
				var typeExpr = expr;
				addCandidate( () -> baseType, expr, () -> typeExpr, defHash);
		}
	}


	public static function getExplicitImportCandidates (ctx:CollectorContext):Map<String, Array<Void->Candidate>> {
		inline function f (ctx:CollectorContext):Map<String, Array<Void->Candidate>> {
			var res = new Map<String, Array<Void->Candidate>>();
			for (i in getImports(ctx)) {
				switch (i) {
					case ImportField(cl, field):
						var e = cl.module + "." + cl.name + "." + field.name;
						var expr = Context.parse(e, cl.pos);
						var defHash = hashFromField.bind(field, cl, e);
						addCandidatesFromField(expr, field.type, res, ExplicitImportInfo.bind(cl, field), defHash);
					case _ :
				}
			}
			return res;
		}

		return switch ctx.localClass() {
			case Some({ id: id }):
				State.instance.getExplicitImportsCache.getOrSet(id, () -> f(ctx));
			case None:
				f(ctx);
		}
	}



	public static function getUsingTypes (ctx:CollectorContext):Array<ClassType> {
		inline function f ():Array<ClassType>
		{
			return [for (t in getLocalUsing()) t.get()];
		}
		return switch ctx.localClass() {
			case Some({id:id}):
				State.instance.getUsingTypesCache.getOrSet(id, () -> f());
			case None:
				f();
		}
	}

	static function mkTypeHashFromExpr(parsed:Expr):TypeHash
	{
		return switch Typer.typeof(parsed) {
			case Some(t):
				mkTypeHash(FastContext.followFast(t));
			case None:
				"#";
		}
	}

	static function hashFromField (f, cl:ClassType, e:String) {
		return switch FastContext.followFast(f.type) {
			case TAbstract(_.get() => { pack : ["scuts", "implicit"], name : "Implicit"}, [_]):
				Context.error("invalid type Implicit for static variable", Context.currentPos());
			case TFun(_, ret):
				mkTypeHash(ret);
			case _:
				var expr = Context.parse(e, cl.pos);
				mkTypeHashFromExpr(expr);
		}
	}

	static function hashFromFieldType (type:Type, e:Expr) {
		return switch FastContext.followFast(type) {
			case TAbstract(_.get() => { pack : ["scuts", "implicit"], name : "Implicit"}, [_]):
				Context.error("invalid type Implicit for static variable", Context.currentPos());
			case TFun(_, ret):
				mkTypeHash(ret);
			case _:
				//var expr = Context.parse(e, pos);
				mkTypeHashFromExpr(e);
		}
	}

	public static function mkTypeHash(type:Type):TypeHash
	{
		inline function hash (id:String, params:Array<Type>, openParams:Array<Bool>) {
			return switch [params, openParams] {
				case [[],[]]:
					id;

					//id + "<" + params.map(mkTypeHash).join(",") + ">";
				case [params, []]:
					id + "<" + params.map(mkTypeHash).join(",") + ">";
				case [params, openParams] if (openParams.length == params.length):
					var hashes = [for (i in 0...params.length) {
						if (openParams[i]) "#" else mkTypeHash(params[i]);
					}];
					//trace(hashes);
					id + "<" + hashes.join(",") + ">";
				case _:
					throw "unexpected";

			}
		}
		return switch FastContext.followFast(type)
		{
			//case TType(t, [p]) if (t.toString() == "scuts.implicit.Open"): "#";
			//case TType(_,_), TLazy(_):
			//	loop(FastContext.followFast(type));
			case TFun([], ret):
				"Void -> " + mkTypeHash(ret);
			case TFun(args, ret):
				args.map( (arg) -> mkTypeHash(arg.t)).join(" -> ") + " -> " + mkTypeHash(ret);
				//"_TFun" + args.length;

			case TInst(t, params):
				var h = switch t.get().kind {
					case KTypeParameter(_):
						"#";
					case _ :

						var openParams = [for (t in t.get().params) {
							switch t.t {
								case TInst(c, p):
									c.get().meta.has(":open");
								case _:
									throw "unexpected";
							}
						}];
						hash(t.toString(), params, openParams);
				};
				h;
			case TEnum(t, params):
				hash(t.toString(), params, []);
			case TAbstract(t, params):
				hash(t.toString(), params, []);
			case t :
				"#";
		}

	}

	public static function createCandidate1 (genExpr:Expr, typeExpr:Void->Expr, type:Void->Type, info:CandidateInfo):Void->Candidate {

		return () -> {

			var typeExpr = typeExpr();
			//trace(Typer.typeof(typeExpr));
			var type = type();
			var rawExpr = genExpr;
			//trace(rawExpr);

			//Typer.preTypeExpr(Typer.asImplicitObject(Typer.preTypeExpr(typeExpr)));
			//var rawExpr = typeExpr;

			var expr = () -> Typer.preTypeExpr(typeExpr);
			//trace(Typer.typeof(expr()));
			var typeFollowed = () -> FastContext.followFast(type);
			var retImplicit = () -> switch typeFollowed() {
				case TFun(args, _):
					var returnedExpr = Typer.getFunctionExprReturnType(expr(), args.length);
					Some(Typer.preTypeExpr(Typer.asImplicitObject(returnedExpr)));
				case _ :
					None;
			};
			// in the member case the typed expression is safe to type but not to generate,
			// because it contains `this` which could have rename metadata attached
			var safeExpr = Tools.lazy(() -> switch info {
				case MemberInfo(_): typeExpr;
				case _: switch type {
					case TFun(args, ret):
						typeExpr;
					case _:
						typeExpr;
				}
			});
			return {
				genExpr: () -> rawExpr,
				type : type,
				typeFollowed: typeFollowed,
				safeExpr: safeExpr,
				expr : expr,
				retImplicit: retImplicit,
				exprImplicit: Tools.lazy(() -> Typer.preTypeExpr(Typer.asImplicitObject(expr()))),
				info : info
			};
		}
	}

	public static function createCandidate (expr, type, info):Void->Candidate {
		var doStore = false;
		var store = None;
		var mkCandidate = () -> {
			var rawExpr = expr;
			var expr = Tools.lazy(() -> Typer.preTypeExpr(expr));
			var typeFollowed = Tools.lazy(() -> FastContext.followFast(type));
			var retImplicit = Tools.lazy(() -> switch typeFollowed() {
				case TFun(args, _):
					if (args.length == 0) {
						doStore = true;
					}
					var returnedExpr = Typer.getFunctionExprReturnType(expr(), args.length);
					Some(Typer.preTypeExpr(Typer.asImplicitObject(returnedExpr)));
				case _ :
					doStore = true;
					None;
			});
			// in the member case the typed expression is safe to type but not to generate,
			// because it contains `this` which could have rename metadata attached
			var safeExpr = Tools.lazy(() -> switch info {
				case MemberInfo(_): rawExpr;
				case _: expr();
			});
			return {
				genExpr: safeExpr,
				type : type,
				typeFollowed: typeFollowed,
				safeExpr: safeExpr,
				expr : expr,
				retImplicit: retImplicit,
				exprImplicit: Tools.lazy(() -> Typer.preTypeExpr(Typer.asImplicitObject(expr()))),
				info : info
			};
		}

		return () -> {
			switch [store, doStore] {
				case [Some(c), true]:
					c;
				case _:
					var c = mkCandidate();
					store = Some(c);
					c;
			}
		}
	}

	static function getPublicStaticClassImplicits (cl:ClassType, includePrivates : Bool, mkCandidateInfo:ClassType->ClassField->CandidateInfo)
	{
		var id = cl.module + cl.name + "_" + includePrivates;

		return State.instance.getPublicStaticClassImplicitsCache.getOrSet(id, () -> {
			var res:StringMap<Array<Void->Candidate>> = new StringMap();

			var statics = cl.statics.get();

			var filtered = statics.filter( x -> (x.isPublic || includePrivates ) && x.meta.has(":implicit"));
			var isEmpty = filtered.length == 0;

			for (f in filtered)
			{
				var e = cl.module + "." + cl.name + "." + f.name;


				var expr = Context.parse(e, cl.pos);
				var defHash = hashFromField.bind(f, cl, e);

				addCandidatesFromField(expr, f.type, res, mkCandidateInfo.bind(cl, f), defHash);


				/*
				var data = createCandidate(expr, f.type, mkCandidateInfo(cl, f));

				if (res.exists(h)) res.get(h).push(data)
				else res.set(h, [data]);
				*/
			}

			{
				sm: res,
				empty: isEmpty,
			}
		});
	}
	/**
	 * Extracts all implicit class fields from fields. The parameter baseExpr is usually
	 * an Expression representing this or the full qualified class. It is used to create
	 * an unambigous Expression to avoid Conflicts with local variables.
	 *
	 * Examples for baseExpr: this, my.full.qualified.ClassA
	 */
	static function getImplicitsFromMeta (fields:Array<ClassField>, baseExpr:Expr, mkInfo:ClassField->CandidateInfo):StringMap<Array<Void->Candidate>>
	{
		if (fields.length == 0) return emptyMap;
		var res:StringMap<Array<Void->Candidate>> = new StringMap();
		for (f in fields)
		{
			switch (f.kind)
			{
				case FieldKind.FVar(_,_) if ( f.meta.has(":implicit")):
					var fname = f.name;
					var expr = macro ${baseExpr}.$fname;
					var defHash = hashFromFieldType.bind(f.type, expr);

					addCandidatesFromField(expr, f.type, res, mkInfo.bind(f), defHash);

					//res.push( createCandidate(expr, f.type, mkInfo(f)));
				case _:
			}
		}
		return res;
	}

	/**
	 * Returns all local candidates
	 */

	static function getLocalCandidates ():Array<Void->Candidate>
	{
		var localVars = FastContext.getLocalTVarsFast();

		var res:StringMap<Array<Void->Candidate>> = new StringMap();

		for (k in localVars.keys())
		{
			var tvar = localVars[k];
			switch (Typer.getUnderlyingTypeOfImplicitType(tvar.t)) {
				case Some(t):
					var expr = macro ${State.instance.helper}.removeImplicit($i{tvar.name});
					var defHash = hashFromFieldType.bind(t, expr);
					addCandidatesFromField(expr, t, res, LocalInfo.bind(k, tvar), defHash);
					//var candidate = createCandidate(expr, t, LocalInfo(k, tvar));
					//all.push(candidate);
				case None:
			}
		}

		return [for (e in res) for (x in e) x];
	}

	/*
	static function getLocalCandidates ():Array<Void->Candidate>
	{
		var localVars = FastContext.getLocalTVarsFast();

		var all = [];

		//var res:StringMap<Array<Void->Candidate>> = new StringMap();

		for (k in localVars.keys())
		{
			var tvar = localVars[k];
			switch (Typer.getUnderlyingTypeOfImplicitType(tvar.t)) {
				case Some(t):
					//trace(t);
					var expr = macro ${State.instance.helper}.removeImplicit($i{tvar.name});
					//trace(tvar);
					//var defHash = hashFromFieldType.bind(t, expr);
					//addCandidatesFromField(expr, t, res, LocalInfo.bind(k, tvar), defHash);
					var candidate = createCandidate(expr, t, LocalInfo(k, tvar));
					all.push(candidate);
				case None:
			}
		}
		return all;
		//return [for (e in res) for (x in e) x];
	}
	*/

	static var emptyMap = new Map<String, Array<Void->Candidate>>();

	public static function preCollectCandidatesFromTypes (ctx:CollectorContext, key:String, types:Array<ClassType>, mkCandidateInfo:ClassType->ClassField->CandidateInfo) {
		inline function f () {
			if (types.length == 0) return emptyMap;
			var res = new Map<String, Array<Void->Candidate>>();

			for ( type in types)
			{
				var all = getPublicStaticClassImplicits(type, false, mkCandidateInfo);

				if (!all.empty) {
					for (k in all.sm.keys()) {
						var a:Array<Void->Candidate> = switch res.get(k) {
							case null:
								var a = [];
								res.set(k, a);
								a;
							case arr: arr;
						}
						for (e in all.sm.get(k)) {
							a.push(e);
						}
					}
				}
			}
			return res;
		}
		return switch ctx.localClass() {
			case Some({id:id}):
				State.instance.preCollectCandidatesFromTypesCache.getOrSet(id + "_" + key, () -> f());
			case None:
				f();
		}
	}

	public static function preFilterCandidates (ids:Void->Array<TypeHash>, candidateMap:Map<String, Array<Void->Candidate>>)
	{
		var res = [];

		for (typeHash in ids()) {

			var candidates = candidateMap.get(typeHash);
			if (candidates != null) for (c in candidates) {
				res.push(c);
			}
		}
		return res;
	}

	static var empty = [];






	/**
	 * Returns all static implicits for the given class.
	 */
	static inline function getStaticCandidates (id, cl:Void->ClassType):Array<Void->Candidate>
	{
		return State.instance.staticCache.getOrSet(id, () -> {
			var all = getPublicStaticClassImplicits(cl(), true, (cl, cf) -> StaticInfo(cf));

			if (all.empty) empty else [for (a in all.sm) for (e in a) e];
		});
	}



	/**
	 * Returns all member implicits for the given class.
	 */
	static inline function getMemberCandidates (id:String, cl:Void->ClassType):Array<Void->Candidate>
	{
		return State.instance.memberCache.getOrSet(id, () -> {
			var all = getImplicitsFromMeta(cl().fields.get(), macro this, MemberInfo);

			[for (a in all) for (e in a) e];
		});
	}


	/**
	 * Returns all implicits (locals, statics, members) for the current context.
	 */
	public static function getImplicitsFromScope (collectorCtx:CollectorContext, withMemberScope:Void->Bool):Scopes
	{
		return {
			locals  : Tools.lazy(() -> getLocalCandidates()),
			statics : Tools.lazy(() -> collectorCtx.localClass().map(cl -> getStaticCandidates(cl.id, cl.classType)).getOrElse(()->empty)),
			members : Tools.lazy(() -> withMemberScope() ? collectorCtx.localClass().map(cl -> getMemberCandidates(cl.id, cl.classType)).getOrElse(()->empty) : empty),
			importMap: Tools.lazy(() -> preCollectCandidatesFromTypes(collectorCtx, "Import", getImportTypes(collectorCtx), ImportInfo)),
			importWildcardMap: Tools.lazy(() -> preCollectCandidatesFromTypes(collectorCtx, "ImportWildcard", getImportWildcardTypes(collectorCtx), ImportInfo)),
			importExplicitMap: Tools.lazy(() -> getExplicitImportCandidates(collectorCtx)),
			usingMap : Tools.lazy(() -> preCollectCandidatesFromTypes(collectorCtx, "Using", getUsingTypes(collectorCtx), ImportInfo)),
		}
	}
}
#end