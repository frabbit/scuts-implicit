package scuts.implicit.macros;

typedef Option<T> = haxe.ds.Option<T>;

class Options {


  public static function getOrElseConst <T>(o:Option<T>, elseValue:T):T return switch (o)
  {
    case Some(v): v;
    case None:    elseValue;
  }

  public static inline function getOrElse <T>(o:Option<T>, elseValue:Void->T):T return switch (o)
  {
    case Some(v): v;
    case None:    elseValue();
  }

  public static inline function map < S, T > (o:Option<S>, f:S->T):Option<T> return switch (o)
  {
    case Some(v): Some(f(v));
    case None:    None;
  }

}

