package scuts.implicit.macros;

#if (macro && (eval || neko))
import haxe.macro.TypeTools;
import haxe.macro.Context;
import haxe.macro.Expr;

import haxe.macro.Type;

using scuts.implicit.macros.Options;

class Typer {

	public static inline function getUnderlyingTypeOfImplicitType (t:Type)
	{
		return switch t {
			case TAbstract(_.toString() => "scuts.implicit.Implicit", [t]):
				Some(t);
			case _ :
				None;
		}
	}

	public static inline function typeof(x:Expr):Option<Type>
	{
		return switch (x.expr) {
			case EMeta({ name : name, params: params }, { expr : EConst(CInt(baseId))}) if (name == ":storedTypedExpr"):
				var id = Std.parseInt(baseId);
				var t = State.instance.typer.typeofCache.get(id);
				if (t == null) {
					var t = FastContext.typeofFast(x);
					State.instance.typer.typeofCache.set(id, t);
					Some(t);
				} else {
					Some(t);
				}
			case _:
				//trace(haxe.macro.ExprTools.toString(x));
				try Some(FastContext.typeofFast(x)) catch (e:Dynamic) {
					if (Context.defined("implicit_debug")) {
						Context.warning(Std.string(e), Context.currentPos());
					}
					None;
				}
		}
	}

	/**
	 * returns a new expr with the parameter type (A) of the function expr e (A->B)
	 */
	public static inline function first (e :Expr):Expr {
		return macro ${State.instance.helper}.first($e);
	}

	public static inline function preTypeExpr (expr) {
		#if (!noStoreExpr)
		return switch (expr.expr) {
			case EMeta({ name : ":storedTypedExpr" },_):
				expr;
			case _ :
				FastContext.storeExprFast(expr);
		}
		//return
		#else
		return expr;
		#end
	}

	public static inline function isTypeable (e:Expr)
	{
		return switch (e.expr) {
			case EMeta({ name : ":storedTypedExpr" },_):
				true;
			case _ :
				FastContext.isTypeableFast(e);
		}
	}

	public static function duplicateType (t:Type):Type {
		return @:privateAccess haxe.macro.Context.load("duplicate_type", 1)(t);
	}
	/**
	 * Checks if the type of expression e is compatible to the type of to.
	 */
	public static inline function isCompatibleWithTypeAsParamExpr (e:Expr, toTypeAsParam:Expr)
	{
		return FastContext.isTypeableFast(macro $toTypeAsParam($e));
	}

	public static function addMonos (t):Type {
		var all = new Map();
		function loop (t) {
			//trace("loop");
			return switch haxe.macro.Context.follow(t) {
				case TInst( _.get() => { name : name, kind : KTypeParameter(_) }, []):
					var cur = all.get(name);
					if (cur == null) {
						var m = Context.getType("scuts.implicit.Mono");
						all.set(name, m);
						m;
					} else {
						cur;
					}
				case t:
					TypeTools.map(t, loop);
			}
		}
		return loop(t);
	}

	public static inline function asParam (e:Expr) {
		return macro ${State.instance.helper}.typeAsParam($e);
	}

	/*
	 * Let the compiler type the function based on the given return type for you.
	 * e.g. if f is typed as T->Array<T> and returnType is of type Array<Int> the resulting expr has type Int->Array<Int>.
	*/
	public static inline function specializeFunctionExpr (f:Expr, returnType:Expr, numArgs:Int)
	{
		return switch (numArgs)
		{
			case 0: macro ${State.instance.helper}.typed0($f, $returnType);
			case 1:
				switch [State.instance.helper.expr, f.expr] {
					case [EMeta({ name : ":storedTypedExpr" },{expr:EConst(CInt(id1))}), EMeta({ name : ":storedTypedExpr" },{ expr: EConst(CInt(id2))})]:
						var func = State.instance.specializeCache.getOrSet(id1 + "_" + id2, () -> {
							preTypeExpr(macro ${State.instance.helper}.typed1.bind($f, _));
						});
						macro $func($returnType);
					case _:
						//trace(f);
						try {
							Context.typeof(f);
						} catch (e:Dynamic) {

							trace("cannot type: " + e);
						}
						macro ${State.instance.helper}.typed1($f, $returnType);
				}
			case 2: macro ${State.instance.helper}.typed2($f, $returnType);
			case 3: macro ${State.instance.helper}.typed3($f, $returnType);
			case 4: macro ${State.instance.helper}.typed4($f, $returnType);
			case 5: macro ${State.instance.helper}.typed5($f, $returnType);
			case 6: macro ${State.instance.helper}.typed6($f, $returnType);
			case _: throw "not implemented";
		}
	}
	public static inline function getFunctionExprReturnType (f:Expr, numArgs:Int)
	{
		return switch (numArgs)
		{
			case 0: macro ${State.instance.helper}.ret0($f);
			case 1: macro ${State.instance.helper}.ret1($f);
			case 2: macro ${State.instance.helper}.ret2($f);
			case 3: macro ${State.instance.helper}.ret3($f);
			case 4: macro ${State.instance.helper}.ret4($f);
			case 5: macro ${State.instance.helper}.ret5($f);
			case 6: macro ${State.instance.helper}.ret6($f);
			case _: throw "not implemented";
		}
	}

	public static inline function sameTypeByImplicit (a:Expr,b:Expr)
	{
		return Typer.isTypeable(macro ${State.instance.helper}.sameType($a, $b));
	}

	public static inline function asImplicitObject (e:Expr)
	{
		return macro ${State.instance.helper}.toImplicitObject($e);
	}

	public static inline function removeLazy (e:Expr)
	{
		return macro ${State.instance.helper}.removeLazy($e);
	}
}
#end