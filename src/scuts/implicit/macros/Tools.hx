package scuts.implicit.macros;

#if (macro && (eval || neko))

import haxe.Timer;
import haxe.Log;
import haxe.CallStack;
import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.Type;
import haxe.PosInfos;

import scuts.implicit.macros.Data;
import scuts.implicit.macros.Typer;

using scuts.implicit.macros.Arrays;

class Tools
{

  public static inline function lazy <X>(f:Void->X):Void->X {
		var r:Null<X> = null;
    var flag = false;
		return () -> {
      return if (flag) {
        r;
      } else {
        r = f();
        flag = true;
        r;
      }
		};
	}
  /**
   * Prints a pretty representation of the type of expression e.
   */
  public static function printTypeOfExpr (e:Expr, ?msg:String, ?pos:PosInfos)
  {
    if (msg == null) msg = "";
    switch (Typer.typeof(e))
    {
      case Some(x): Log.trace(msg + ":" + prettyType(x), pos);
      case None: Log.trace("cannot type the expression " + prettyExpr(e), pos);
    }
  }

  /**
   * Prints a pretty representation of all types of the expressions in exprs.
   */
  public static function printTypeOfExprs (exprs:Array<Expr>, ?msg:String, ?pos:PosInfos)
  {
    if (msg == null) msg = "";
    var res = exprs.map(function (x) return prettyTypeOfExpr(x));
    trace(msg + ":" + res.join(","), pos);
  }

  /**
   * Returns a pretty String representation of expression e.
   */
  public static function prettyExpr (x:Expr)
  {
    var first = haxe.macro.ExprTools.toString(x);
    return first.split("scuts.implicit.macros.ImplicitsHelper.").join("_");
  }

  /**
   * Prints a pretty representation of the expression e.
   */
  public static function printExpr (x:Expr, ?msg:String, ?pos:PosInfos)
  {
    if (msg == null) msg = "";
    trace(msg + ":" + prettyExpr(x), pos);
  }

  /**
   * Returns a pretty string representation of all expressions in exprs.
   */
  public static function printExprs (x:Array<Expr>, ?msg:String, ?pos:PosInfos)
  {
    if (msg == null) msg = "";
    trace(msg + ":" + x.map(r -> prettyExpr(r)).join(","), pos);
  }

  public static function prettyExprs (x:Array<Expr>)
  {
    return x.map(r -> prettyExpr(r)).join(",");
  }

  /**
   * Returns a pretty String representation of type of expression e.
   */
  public static function prettyTypeOfExpr (e:Expr) return switch (Typer.typeof(e))
  {
    case Some(x): prettyType(x);
    case None: "(Cannot Type Expression)";
  }

  /**
   * Prints a pretty representation of the type t.
   */
  public static function printType (t:Type, ?msg:String, ?pos:PosInfos)
  {
    if (msg == null) msg = "";
    trace(msg + ":" + prettyType(t), pos);
  }

  /**
   * Returns a pretty String representation of the type t.
   */
  public static function prettyType (t:Type)
  {
    var first = haxe.macro.TypeTools.toString(t);
    var pretty = first
      .split("scuts.ht.core.Of").join("Of")
      .split("scuts.ht.core._").join("_")
      .split("StdTypes.").join("");
      //.split(Print.UNKNOWN_T_MONO).join("Unknown");

    return pretty;
  }

  public static function countMonos (t:Type)
  {
    return 0;
    return switch Context.follow(t) {
      case TInst(a, tl):
        Arrays.sum(tl, countMonos);
      case TEnum(a, tl):
        Arrays.sum(tl, countMonos);
      case TAnonymous(fields):

        Arrays.sum([for (f in fields.get().fields) f.type], countMonos);
      case TDynamic(_):
        0;
      case TAbstract(a, tl):
        Arrays.sum(tl, countMonos);
      case TType(a, tl):
        Arrays.sum(tl, countMonos);
      case TFun(args, ret):
        Arrays.sum([for (a in args) a.t], countMonos) + countMonos(ret);

      case TMono(ref):
        var ref = ref.get();
        if (ref == null) 1 else countMonos(ref);
      case TLazy(l): countMonos(l());


    }
  }

  public static var ansiColorRed = "\033[0;31m";
  public static var ansiColorGreen = "\033[0;32m";
  public static var ansiColorDef = "\033[0;37m";

  public static function makeRed (c:String) {
    return ansiColorRed + c + ansiColorDef;
  }
  public static function makeGreen (c:String) {
    return ansiColorGreen + c + ansiColorDef;
  }
}

#end