package scuts.implicit.macros;

#if (macro && (eval || neko))

using scuts.implicit.macros.Options;

import haxe.macro.Expr;
import haxe.macro.Type;

typedef TypeHash = String;

typedef ScopeExpr = { scope : Scope, expr : Expr }

typedef NamedExpr = { name: String, expr:Expr }

typedef StackItem = { id:Void->String, required : Expr, requiredImplicit: Expr, candidate : Candidate, ctx:ResolveContext };

typedef ResolveStack = Array<StackItem>;

enum CheckCircularDependencyResult {
  Error(e:ResolverError);
  LazyExpression(e:Expr);
  NoCircularDependency;
}

typedef ResolveContext = {
  cleanRequired:Void->Expr,
	scopes : Scopes,
	stack : ResolveStack,
  type:Void->Option<Type>,
  id:Void->String,
  required : Expr,
  ids:Void->Array<TypeHash>,
  idsLookup:Void->Map<String, Bool>,
  requiredImplicit:Expr,
  requiredAsParam: Expr,
  collectorCtx: CollectorContext,
  lazyUsed:Bool,
  isLazy:Bool,
  isArg:Bool,
  depth:Int,
}

typedef CollectorContext = {
  localClass: Void->Option<{id : String, classType: Void->ClassType}>,
}

enum ResolverError
{
  NoImplicitObjectInContext (required:Expr);
  CircularDependency (stackItem:StackItem, ctx:ResolveContext);
  FunctionExprIsNotAFunction (functionExpr:Expr);
  FunctionExprCannotBeTyped (functionExpr:Expr);
  ImplicitAmbiguityError(scope:Scope, arr:Array<Candidate>, resolveContext:ResolveContext);
  UnexpectedError(err:String);
}

/**
 * Scopes provides access for implicit objects from different scopes.
 */
typedef Scopes =
{
  locals : Void -> Array<Void->Candidate>,
  members : Void -> Array<Void->Candidate>,
  statics : Void -> Array<Void->Candidate>,
  importMap : Void -> Map<String, Array<Void->Candidate>>,
  importWildcardMap: Void -> Map<String, Array<Void->Candidate>>,
  importExplicitMap: Void -> Map<String, Array<Void->Candidate>>,
  usingMap: Void -> Map<String, Array<Void->Candidate>>,
}

enum PluginStage {
	BeforeAll;
	BeforeMembers;
	BeforeStatics;
	BeforeExplicitImports;
	BeforeUsings;
	BeforeWildcardImports;
	BeforeImports;
	AfterAll;
}
/**
 * A Scope represents different contexts for implicit resolution
 */
enum Scope
{
  Plugin(stage:PluginStage);
  Local; // Local Context
  Member; // Member Context
  Static; // Static Context
  ExplicitImport; // Explicit Imports like my.pack.Class.myFunc
  Using; // Using Context
  Import; // Import Scope
  WildcardImport;
}

enum Import {
  ImportClass(ct:ClassType);
  ImportField(ct:ClassType, field:ClassField);
  ImportWildcard(ct:ClassType);
}

enum CandidateInfo {
  LocalInfo (name:String, tf:TVar);
  MemberInfo (field:ClassField);
  StaticInfo (field:ClassField);
  UsingInfo (cl :ClassType, field:ClassField);
  ImportInfo (cl:ClassType, field:ClassField);
  ExplicitImportInfo (cl:ClassType, field:ClassField);
  PluginInfo;
}

enum ResolveResult {
  RRSuccess(e:Expr, c:Candidate);
  RRNotFound;
  RRFailure(f:ResolverError);
}

class ResolveResults {
  public static inline function ifNotFound (rr:ResolveResult, f:Void->ResolveResult) {
    return switch rr {
      case RRNotFound: f();
      case x : x;
    }
  }


}


typedef Candidate = {
  genExpr: Void->Expr,
  typeFollowed : Void->Type,
  expr : Void->Expr,
  safeExpr: Void->Expr, // while expr can be a stored typed expression it's safe to type, but `this` calls can have rename metadata attached and member candidates are cached
  exprImplicit: Void->Expr,
  retImplicit: Void->Option<Expr>,
  type : Type,
  info : CandidateInfo
};

class Candidates {
  public static function getColoredNameWithType (c:Candidate) {
    return Tools.makeGreen(getName(c)) + " : " + Tools.makeRed(getType(c));
  }

  static function fqName (cl:ClassType, field:ClassField) {
    var cn = if (StringTools.endsWith(cl.module, cl.name)) cl.module else cl.module + "." + cl.name;
    return cn + "." + field.name;
  }

  static function getType (c:Candidate) {
    function mkInfo (exprString:String) {
      var expr = haxe.macro.Context.parse('$exprString', haxe.macro.Context.currentPos() );
      return Tools.prettyTypeOfExpr(expr);
    }


    return switch c.info {
      case LocalInfo(name,tvar):
        Tools.prettyType(c.type);
      case MemberInfo(field): Tools.prettyType(field.type);
      case StaticInfo(field): Tools.prettyType(field.type);
      case UsingInfo(cl, field):
        var fq = fqName(cl, field);
        mkInfo(fq);
      case ImportInfo(cl, field):
        var fq = fqName(cl, field);
        mkInfo(fq);
      case ExplicitImportInfo(cl,field):
        var fq = fqName(cl, field);
        mkInfo(fq);
      case PluginInfo:
        "";
        Tools.prettyType(c.type);
    }
  }

  public static function getName (c:Candidate) {

    return switch c.info {
      case LocalInfo(name,tvar):
        name;
      case MemberInfo(field):
        "this." + field.name;
      case StaticInfo(field):
        "::" + field.name;
      case UsingInfo(cl, field):
        fqName(cl, field);
      case ImportInfo(cl, field):
        fqName(cl, field);

      case ExplicitImportInfo(cl,field):
        fqName(cl, field);
      case PluginInfo:
        "from plugin";
    }
  }
}

#end