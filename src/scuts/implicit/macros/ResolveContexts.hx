package scuts.implicit.macros;

import haxe.ds.StringMap;

import haxe.macro.TypeTools;

import scuts.implicit.macros.Cache;
import scuts.implicit.macros.Errors;
import scuts.implicit.macros.Typer;

import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.Type;
import haxe.macro.ExprTools;

import scuts.implicit.macros.Data;

using scuts.implicit.macros.Arrays;
using scuts.implicit.macros.Validations;
using scuts.implicit.macros.Options;

using scuts.implicit.macros.Data.ResolveResults;
class ResolveContexts {

	public static function create (getRequired : Void -> Expr, getCleanRequired:Void->Expr, type:Option<Type>) {

		var localClass = switch FastContext.getLocalClassFast() {
			case null: None;
			case cl:
				var id = cl.toString();
				Some({ id : cl.toString(), classType: Tools.lazy(cl.get)});
		}
		var collectorCtx = {
			localClass: () -> localClass,
		};
		var isMemberScope = Tools.lazy(() -> Typer.isTypeable(macro @:pos(FastContext.currentPosFast()) this));

		var cleanRequired = () -> getCleanRequired();

		var required = getRequired();
		var requiredImplicit = Typer.preTypeExpr(Typer.asImplicitObject(required));

		var ids = Tools.lazy( () -> switch (type) {
			case Some(t): TypeHashTools.getIdsForType(t);
			case None: TypeHashTools.getIdsForExpr(required);
		});
		var type = Tools.lazy( () -> switch (type) {
			case Some(t): Some(t);
			case None: Typer.typeof(required);
		});
		var ctx = {
			cleanRequired : cleanRequired,
			collectorCtx: collectorCtx,
			ids: ids,
			idsLookup: Tools.lazy( () -> [for (k in ids()) k => true]),
			type: type,
			id: Tools.lazy( () -> ids().join(",")),
			scopes : Collector.getImplicitsFromScope(collectorCtx, isMemberScope),
			required : required,
			requiredImplicit: requiredImplicit,
			requiredAsParam: Typer.preTypeExpr(Typer.asParam(requiredImplicit)),
			stack : [],
			lazyUsed:false,
			isLazy:false,
			isArg:false,
			depth: 0,
		};
		return ctx;
	}

	public static function createForFunctionArg (functionExpr:Expr, cleanFunctionExpr:Void->Expr, rawFuncExpr:Expr, numParams:Int,
		ctx:ResolveContext, newStack:ResolveStack, newArgs:Array<Expr>, i:Int) {
		// partially apply the current function with all arguments except the current (_) to get the required type for this argument.
		// replace placeholders with null, we need a function with exactly one
		// parameter and as much as possible arguments applied.
		// the first if else case is just a small optimization for case where we don't need to partial apply
		//var lastId = "__resolved" + ctx.depth;

		var ids = [for (d in 0...ctx.depth+1) {
			var id = "__resolved" + d;
			macro var $id = null;
		}];

		var mkPartial = fExpr -> {

			var x = if (numParams == 1 && newArgs.length == 0) { // optimization case
				fExpr;
			} else {
				var newArgsExpanded = newArgs.copy();
				newArgsExpanded.push(macro _);
				for (_ in i+1...numParams) {
					newArgsExpanded.push(macro throw "error");
				}
				var all = ids.concat([macro $fExpr.bind($a{newArgsExpanded})]);
				macro $b{all};

			}
			//trace(ExprTools.toString(x));
			return x;
		};

		//trace("create this");
		var cleanRequired = () -> Typer.first(mkPartial(cleanFunctionExpr()));

		var partialApplied = Typer.first(mkPartial(functionExpr));

		var required = Typer.preTypeExpr(partialApplied);

		var isLazy = isLazy(required);
		//trace(required);
		//trace(isLazy);
		var required = if (isLazy) removeLazy(required) else required;

		var requiredImplicit = Typer.preTypeExpr(Typer.asImplicitObject(required));
		var ids = Tools.lazy( () -> TypeHashTools.getIdsForExpr(required));
		var type = Tools.lazy( () -> Typer.typeof(required));

		var ctx = {
			isArg:true,
			cleanRequired : cleanRequired,
			collectorCtx: ctx.collectorCtx,
			ids: ids,
			type: type,
			id: Tools.lazy( () -> ids().join(",")),
			idsLookup: Tools.lazy( () -> [for (k in ids()) k => true]),
			required : required,
			requiredImplicit: requiredImplicit,
			requiredAsParam: Typer.preTypeExpr(Typer.asParam(requiredImplicit)),
			scopes : ctx.scopes,
			stack : newStack,
			lazyUsed:false,
			isLazy:isLazy,
			depth: ctx.depth + 1,
		};
		return ctx;
	}

	static function removeLazy (expr:Expr) {
		return switch Typer.typeof(expr) {
			case Some(TFun( [{ t:TEnum(t,[]) }], _)) if (t.toString() == "scuts.implicit.Unit"):
				Typer.preTypeExpr(Typer.removeLazy(expr));
			case _:
				expr;
		};
	}

	static function isLazy (expr:Expr) {
		return switch Typer.typeof(expr) {
			case Some(TFun( [{ t:TEnum(t,[]) }], _)) if (t.toString() == "scuts.implicit.Unit"):
				true;
			case t:
				//trace(t);
				false;
		};
	}

	public static function restoreMonos (ctx:ResolveContext) {
		//trace("restore monos");
		var required = Typer.preTypeExpr(ctx.cleanRequired());

		var required = if (ctx.isLazy) removeLazy(required) else required;

		var requiredImplicit = Typer.preTypeExpr(Typer.asImplicitObject(required));
		var requiredAsParam = Typer.preTypeExpr(Typer.asParam(requiredImplicit));
		return {
			cleanRequired: ctx.cleanRequired,
			collectorCtx: ctx.collectorCtx,
			ids: ctx.ids,
			type: ctx.type,
			id: ctx.id,
			idsLookup: ctx.idsLookup,
			required: required,
			requiredImplicit: requiredImplicit,
			requiredAsParam: requiredAsParam,
			scopes: ctx.scopes,
			stack: ctx.stack,
			lazyUsed:ctx.lazyUsed,
			isLazy:ctx.isLazy,
			isArg:ctx.isArg,
			depth: ctx.depth,
		}
	}
}