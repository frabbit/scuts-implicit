package scuts.implicit.macros;

import haxe.ds.Option;
import scuts.implicit.macros.Typer;

enum Result<X> {
    Permutations(t:X, perms:Array<Result<X>>);
    Wildcard;
}

typedef MyType = {
	t: haxe.macro.Type,
	id : Void->String,
	params: Void->Array<MyType>,
}

class TypeHashTools {

	static inline function reallyGetTypeId (t:haxe.macro.Type) {
		return switch (t) {
			case TInst(t, params):
				t.toString();
				/*
				var h = switch t.get().kind {
					case KTypeParameter(_):
						trace(t.get().kind);
						trace(t.toString());
						"_";
					case _ :
						t.toString();
				};
				h;
				*/
			case TEnum(t, _):
				t.toString();
			case TAbstract(t, _):
				t.toString();
			case TFun(args, ret):
				""; // id consists of args and return type
			case t:
				"#";
		}
	}

	static inline function getTypeId (t:MyType) {
		return t.id();

	}

	static function typeToMyType (p:haxe.macro.Type):MyType {
		var followed = Tools.lazy(() -> FastContext.followFast(p));
		return {
			t : p,
			id : Tools.lazy( () -> reallyGetTypeId(followed()) ),
			params: Tools.lazy( () -> reallyGetParams(followed()) ),
		};
	}


	static var empty = [];

	static inline function reallyGetParams (t:haxe.macro.Type) {

		return switch (t) {
			case TInst(_, []) | TEnum(_, []) | TAbstract(_, []): // optimization
				empty;
			case TInst(_, params):
				params.map(typeToMyType);
			case TEnum(_, params):
				params.map(typeToMyType);
			case TAbstract(_, params):
				params.map(typeToMyType);
			case TFun(args, ret):
				var res = [for (a in args) {
					typeToMyType(a.t);
				}];
				res.push(typeToMyType(ret));
				res;
			case t:
				empty;
		}
	}

	static inline function getParams (t:MyType) {
		return t.params();
	}

	static var combineEmpty = [];
    static inline function combine <X>(t:X, a:Array<Result<X>>, b:Array<Result<X>>):Array<Result<X>> {
        return switch [a, b] {
            case [[], []]: combineEmpty;
            case [a, b]:
            	[for (a1 in a) for (b1 in b) Permutations(t, [a1, b1])];
        }

    }

	static function createResult (t:MyType, depth:Int = 0):Array<Result<MyType>> {

        return switch (getParams(t)) {
            case []: [Wildcard, Permutations(t, [])];
            case [a]:
            	var resA = createResult(a, depth+1);
				var res = [for (x in resA) {
					Permutations(t, [x]);
				}];
				res.push(Wildcard);
				res;
          	case x:
			  	//trace(x.length);
				//trace(t);
				var res = [];
				var all = [];
				for (j in 0...x.length) {
					var cur = createResult(x[j], depth+1);
					all[j] = cur;
				}

				function loop (index:Int, acc:Array<Result<MyType>>) {
					switch index {
						case i if (i == x.length):
							res.push(Permutations(t, acc));
						case _:
							for (i in all[index]) {
								var acc = acc.concat([i]);
								loop(index + 1, acc);
							}
					}
				}
				loop(0, []);

				res.push(Wildcard);
				res;

        }

    }
    static function resultToIds (perms:Array<Result<MyType>>):Array<String> {
		var res = [];
        function loop (p, depth:Int):String {
            return switch (p) {
				case Wildcard:
					"#";
				case Permutations(t, perms):
					switch perms {
						case []:
							getTypeId(t);
						case _:
							switch (t.t) {

								case TFun(_, _):
									var pString = "";
									var first = true;
									for (p in perms) {
										if (first) {
											first = false;
											pString += loop(p, depth+1);
										} else {
											pString += " -> " + loop(p, depth+1);
										}
									}

									var res = if (perms.length == 1) "Void -> " + pString else pString;
									res;


								case _:
									var pString = "";
									var first = true;
									for (p in perms) {
										if (first) {
											first = false;
											pString += loop(p, depth+1);
										} else {
											pString += "," + loop(p, depth+1);
										}
									}
									if (depth == 0) getTypeId(t) + "<"+ pString + ">" else getTypeId(t) + "<" + pString + ">";

							}

					}
            }
        }
		//trace(perms);
        return [for (p in perms) loop(p, 0)];
    }

	public static inline function getIdsForExpr (expr:haxe.macro.Expr) {
		var type = FastContext.typeofFast(expr);
		return getIdsForType(type);
	}

	public static inline function getIdsForType (type:haxe.macro.Type) {
		var ids = resultToIds(createResult(typeToMyType(type)));

		// hack to avoid duplicates
		var h = new Map();
		for (i in ids) h.set(i, true);
		return [for (k in h.keys()) k];
		//return ids;
	}
}