package scuts.implicit.macros;

#if macro

import haxe.macro.Type;
import haxe.macro.Expr;
import haxe.ds.Option;
import haxe.macro.Context;
import haxe.ds.StringMap;
import scuts.implicit.macros.Data;

typedef StringMapWithEmptyFlag<T> = {
	sm:StringMap<T>,
	empty:Bool,
}

typedef StateInstance = {
	typer : {
		typeofCache:Map<Int, Type>,
	},
	specializeCache:Cache<Expr>,
	helper:Expr,
	getPublicStaticClassImplicitsCache:Cache<StringMapWithEmptyFlag<Array<Void->Candidate>>>,
	importCache:Cache<Array<Import>>,
	getImportTypesCache:Cache<Array<ClassType>>,
	getImportWildcardTypesCache:Cache<Array<ClassType>>,
	getExplicitImportsCache:Cache<Map<String, Array<Void->Candidate>>>,
	getUsingTypesCache:Cache<Array<ClassType>>,
	preCollectCandidatesFromTypesCache:Cache<Map<String, Array<Void->Candidate>>>,
	staticCache:Cache<Array<Void->Candidate>>,
	memberCache:Cache<Array<Void->Candidate>>,
	scutsImplicit:Expr,
	plugins: {
		beforeAll:Array<ResolveContext->Option<Expr>>,
		beforeMembers:Array<ResolveContext->Option<Expr>>,
		beforeStatics:Array<ResolveContext->Option<Expr>>,
		beforeExplicitImports:Array<ResolveContext->Option<Expr>>,
		beforeUsing:Array<ResolveContext->Option<Expr>>,
		beforeWildcardImport:Array<ResolveContext->Option<Expr>>,
		beforeImport:Array<ResolveContext->Option<Expr>>,
		afterAll:Array<ResolveContext->Option<Expr>>,
	},
	allScopes: Array<Scope>,
}

@:access(haxe.macro.Context.load)
class State {

	static var init = {
		haxe.macro.Context.onMacroContextReused( () -> {
			trace("reuse state");
			instance = createInstance();
			return true;
		});
	}

	static function createInstance () {
		var cacheEnabled = true;
		return {
			typer: {
				typeofCache : new Map<Int, Type>(),
			},
			specializeCache: new Cache(cacheEnabled),
			getPublicStaticClassImplicitsCache: new Cache(cacheEnabled),
			importCache: new Cache<Array<Import>>(cacheEnabled),
			getImportTypesCache: new Cache(cacheEnabled),
			getImportWildcardTypesCache: new Cache(cacheEnabled),
			getExplicitImportsCache: new Cache(cacheEnabled),
			getUsingTypesCache: new Cache<Array<ClassType>>(cacheEnabled),
			preCollectCandidatesFromTypesCache: new Cache(cacheEnabled),
			staticCache: new Cache(cacheEnabled),
			memberCache: new Cache(cacheEnabled),

			helper: Typer.preTypeExpr(macro scuts.implicit.macros.Helper),
			scutsImplicit : Typer.preTypeExpr(macro scuts.implicit.Implicit),
			plugins : {
				beforeAll: [],
				beforeMembers: [],
				beforeStatics: [],
				beforeExplicitImports: [],
				beforeUsing: [],
				beforeWildcardImport: [],
				beforeImport: [],
				afterAll: [],
			},
			allScopes: [
				Local,
				Member,
				Static,
				ExplicitImport,
				Using,
				WildcardImport,
				Import
			]
		};
	}

	public static inline function getPluginArray (stage) {
		return switch stage {
			case BeforeAll:
				instance.plugins.beforeAll;
			case BeforeMembers:
				instance.plugins.beforeMembers;
			case BeforeStatics:
				instance.plugins.beforeStatics;
			case BeforeExplicitImports:
				instance.plugins.beforeExplicitImports;
			case BeforeUsings:
				instance.plugins.beforeUsing;
			case BeforeWildcardImports:
				instance.plugins.beforeWildcardImport;
			case BeforeImports:
				instance.plugins.beforeImport;
			case AfterAll:
				instance.plugins.afterAll;
		};
	}

	public static function addPlugin (stage, plugin) {
		var arr = getPluginArray(stage);
		arr.push(plugin);
		updateScopes();
	}

	static function updateScopes () {
		var scopes = [];
		var plugins = instance.plugins;
		if (plugins.beforeAll.length > 0) scopes.push(Plugin(BeforeAll));
		scopes.push(Local);
		if (plugins.beforeMembers.length > 0) scopes.push(Plugin(BeforeMembers));
		scopes.push(Member);
		if (plugins.beforeStatics.length > 0) scopes.push(Plugin(BeforeStatics));
		scopes.push(Static);
		if (plugins.beforeExplicitImports.length > 0) scopes.push(Plugin(BeforeExplicitImports));
		scopes.push(ExplicitImport);
		if (plugins.beforeUsing.length > 0) scopes.push(Plugin(BeforeUsings));
		scopes.push(Using);
		if (plugins.beforeWildcardImport.length > 0) scopes.push(Plugin(BeforeWildcardImports));
		scopes.push(WildcardImport);
		if (plugins.beforeImport.length > 0) scopes.push(Plugin(BeforeImports));
		scopes.push(Import);
		if (plugins.afterAll.length > 0) scopes.push(Plugin(AfterAll));
		instance.allScopes = scopes;

	}

	public static var instance:StateInstance = createInstance();

}

#end