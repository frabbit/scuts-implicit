package scuts.implicit.macros;

enum Validation < F, S > {
  Failure(f:F);
  Success(s:S);
}

class Validations
{

  /**
   * Maps the right side of an Validation with f and flattens the result.
   */
  public static inline function flatMap <F,S,SS> (o:Validation<F,S>, f:S->Validation<F, SS>):Validation<F,SS> return switch o
  {
    case Failure(f): Failure(f);
    case Success(v): f(v);
  }

  /**
   * Maps the success value of an Validation and returns a new Validation based on the mapping function f.
   */
  public static inline function map < F,S,SS > (v:Validation<F,S>, f:S->SS):Validation<F,SS> return switch v
  {
    case Failure(f): Failure(f);
    case Success(s): Success(f(s));
  }

}
