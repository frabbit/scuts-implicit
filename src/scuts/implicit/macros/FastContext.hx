package scuts.implicit.macros;

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

@:access(haxe.macro.Context.load)
class FastContext {
	static var _storeExprFast = (Context.load("store_expr", 1):Expr->Expr);
	static var _storeTypedExprFast = (Context.load("store_typed_expr", 1):TypedExpr->Expr);
	static var _followFast = (Context.load("follow", 2):Type->Bool->Type);
	static var _typeofFast = (Context.load("typeof", 1):Expr->Type);
	static var _getLocalTypeFast = (Context.load("get_local_type", 0):Void->Type);
	static var _currentPosFast = (Context.load("current_pos", 0):Void->haxe.macro.Position);
	static var _getExpectedTypeFast = (Context.load("get_expected_type", 0):Void->Null<Type>);
	static var _getLocalTVarsFast = (Context.load("local_vars", 1):Bool -> Map<String,haxe.macro.Type.TVar>);
	#if isTypeable
	static var _isTypeableFast = (Context.load("is_typeable", 1):Expr->Bool);
	#end
	/**
	 * Checks if the expression e is typeable by the compiler.
	 */

	public static inline function storeExprFast (e:Expr) {
		return _storeExprFast(e);
	}


	public static inline function storeTypedExprFast (e:TypedExpr):Expr {
		return _storeTypedExprFast(e);
	}

	public static inline function followFast (e:Type):Type {
		return _followFast(e, false);
	}


	public static inline function typeofFast (e:Expr):Type {
		return _typeofFast(e);
	}

	public static inline function getLocalClassFast() : Null<haxe.macro.Type.Ref<haxe.macro.Type.ClassType>> {
		var l  = _getLocalTypeFast();
		if( l == null ) return null;
		return switch( l ) {
		case TInst(c,_): c;
		default: null;
		}
	}

	public static inline function currentPosFast() : haxe.macro.Position {
		return _currentPosFast();
	}

	public static inline function getExpectedTypeFast ():Null<Type> {
		return _getExpectedTypeFast();
	}

	public static inline function getLocalTVarsFast() : Map<String,haxe.macro.Type.TVar> {
		return _getLocalTVarsFast(true);
	}

	#if isTypeable
	public static inline function isTypeableFast (e:Expr) {
		return _isTypeableFast(e);
	}
	#else
	public static inline function isTypeableFast (e:Expr) {
		return try {typeofFast(e); true;} catch (_:Dynamic) false;
	}
	#end
}