package scuts.implicit.macros;

import haxe.macro.Context;
import haxe.macro.Type;

class Instances {
	public static function buildAll () {
		var internal = Context.getType("scuts.implicit.InstancesInternal");
		switch [Context.getLocalType(), internal] {
			case [t = TInst(_, tl), TType(x, _)] if (tl.length > 1):
				var last = tl.pop();
				var id = 1;
				var args = [for (t in tl) { t : t, name : "a" + id++, opt: false}];
				var void = Context.getType("Void");
				var tfun = TFun(args, void);


				return TType(x, [tfun, last]);
			case _:
				Context.error("Class expected", Context.currentPos());
			}

		return null;
	}
}