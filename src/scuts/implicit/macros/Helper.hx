package scuts.implicit.macros;

extern class ImplicitObject<X> {}

extern class Helper
{
  #if !macro

  public static function toImplicitObject  <A>            (a:A)                  :ImplicitObject<A> return throw "error";

  public static function first <A,B> (f1:A->B):A return throw "error";

  public static function typeAsParam <A> (f2:A):A->Void return throw "error";

  public static function ret0 <B>(f3:Void->B):B return throw "error";
  public static function ret1 <A,B>(f4:A->B):B return throw "error";
  public static function ret2 <A,B,C>(f5:A->B->C):C return throw "error";
  public static function ret3 <A,B,C,D>(f6:A->B->C->D):D return throw "error";
  public static function ret4 <A,B,C,D,E>(f7:A->B->C->D->E):E return throw "error";
  public static function ret5 <A,B,C,D,E,F>(f8:A->B->C->D->E->F):F return throw "error";
  public static function ret6 <A,B,C,D,E,F,G>(f9:A->B->C->D->E->F->G):G return throw "error";

  public static function typed0 <B>(f10:Void->B, x:B):Void->B return f;
  public static function typed1 <A,B>(f11:A->B,x:B):A->B return f;
  public static function typed2 <A,B,C>(f:A->B->C,x:C):A->B->C return f;
  public static function typed3 <A,B,C,D>(f:A->B->C->D, x:D):A->B->C->D return f;
  public static function typed4 <A,B,C,D,E>(f:A->B->C->D->E, x:E):A->B->C->D->E return f;
  public static function typed5 <A,B,C,D,E,F>(f:A->B->C->D->E->F, x:F):A->B->C->D->E->F return f;
  public static function typed6 <A,B,C,D,E,F,G>(f:A->B->C->D->E->F->G, x:G):A->B->C->D->E->F->G return f;

  public static function sameType <T>(a:ImplicitObject<T>, b:ImplicitObject<T>):Bool return true;

  public static inline function removeImplicit <A>(a:scuts.implicit.Implicit<A>):A return a;
  public static inline function removeLazy <A>(a:scuts.implicit.Lazy<A>):A return a();

  #end


}