package scuts.implicit;

#if macro

import scuts.implicit.macros.State;
import scuts.implicit.macros.Data;
import haxe.ds.Option;
import haxe.macro.Expr;

class Plugin {
	public static function register (f:ResolveContext->Option<Expr>, stage:PluginStage) {
		State.addPlugin(stage, f);
	}
}
#end